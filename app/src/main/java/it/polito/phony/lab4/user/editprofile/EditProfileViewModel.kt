package it.polito.phony.lab4.user.editprofile

import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ThumbnailUtils
import android.os.Build
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.polito.phony.lab4.user.account.AccountViewModel
import it.polito.phony.lab4.user.entity.User
import it.polito.phony.lab4.user.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period
import java.util.*

class EditProfileViewModel(user: User?, img: Bitmap?, private val userRepository: UserRepository) : ViewModel() {

    companion object {
        const val TAG = "EditProfileViewModel"
        const val INVALID_NAME = "Please insert a valid name"
        const val INVALID_EMAIL = "Please insert a valid email"
        const val INVALID_BIRTHDATE = "You must be 18+ years old"
        const val INVALID_LOCATION = "Please insert a valid location"
        const val INVALID_PHONE_NUM = "Please insert a valid phone number"
        const val SELECT_IMG = "Please select a valid picture"
        const val SAVE_FAIL = "User profile save failed"
        const val MINIMUM_AGE = 18
    }

    val name = MutableLiveData(user?.name)
    val birthdate = MutableLiveData(user?.birthdate)
    val nickname = MutableLiveData(user?.nickname)
    val email = MutableLiveData(user?.email)
    val phonenumber = MutableLiveData(user?.phonenumber)
    val markerLat = MutableLiveData<Double>(user?.markerLat)
    val markerLong = MutableLiveData<Double>(user?.markerLong)
    val location = MutableLiveData<String>(user?.location)

    private val _user by lazy {
        return@lazy MutableLiveData<User?>(user)
    }
    val user: LiveData<User?>
        get() = _user

    private val _img by lazy {
        return@lazy MutableLiveData<Bitmap?>(img)
    }
    val img: LiveData<Bitmap?>
        get() = _img

    private val _isImgBeenEdited by lazy {
        return@lazy MutableLiveData(false)
    }
    val isImgBeenEdited: LiveData<Boolean>
        get() = _isImgBeenEdited

    private val _currentPhotoPath by lazy {
        return@lazy MutableLiveData<String?>()
    }
    val currentPhotoPath: LiveData<String?>
        get() = _currentPhotoPath

    private val _toastmsg by lazy {
        return@lazy MutableLiveData<String?>()
    }
    val toastmsg: LiveData<String?>
        get() = _toastmsg

    private val _snackmsg by lazy {
        return@lazy MutableLiveData<String?>()
    }
    val snackmsg: LiveData<String?>
        get() = _snackmsg

    private val _isSaving by lazy {
        return@lazy MutableLiveData(false)
    }
    val isSaving: LiveData<Boolean>
        get() = _isSaving

    private val _isSaved by lazy {
        return@lazy MutableLiveData(false)
    }
    val isSaved: LiveData<Boolean>
        get() = _isSaved

    fun onSaveProfile(newUser: User, newImg: Bitmap?) {

        if (!isValidName()) {
            _toastmsg.value = INVALID_NAME
        } else if (!isValidBirthDate()) {
            _toastmsg.value = INVALID_BIRTHDATE
        } else if (!isValidEmail()) {
            _toastmsg.value = INVALID_EMAIL
        } /*else if (!isValidLocation()) {
            _toastmsg.value = INVALID_LOCATION
        }*/ else if (!isValidPhoneNumber()) {
            _toastmsg.value = INVALID_PHONE_NUM
        } else {

            viewModelScope.launch(context = Dispatchers.IO) {
                _isSaving.postValue(true)
                _isSaved.postValue(false)
                _user.value?.id?.let { id ->
                    val dataSaved = userRepository.setUserData(id, newUser)
                    var imgSaved = true
                    newImg?.let { img ->
                        imgSaved = userRepository.setUserImg(id, img)
                    }

                    if (!dataSaved || !imgSaved) {
                        _snackmsg.postValue(SAVE_FAIL)
                    }
                }
                _isSaving.postValue(false)
                _isSaved.postValue(true)
            }
        }
    }

    fun onSaveProfileComplete() {
        _isSaved.value = false
    }


    fun onShowToastMsgComplete() {
        _toastmsg.value = null
    }

    fun onShowSnackMsgComplete() {
        _snackmsg.value = null
    }

    private fun isValidEmail(): Boolean {
        if (email.value.isNullOrBlank()) {
            return false
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email.value.toString()).matches()
    }

    private fun isValidBirthDate(): Boolean {
        if (birthdate.value.isNullOrBlank()) {
            return false
        }
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val date = LocalDate.parse(birthdate.value)
            val today = LocalDate.now()
            (date.isBefore(today) && !date.isAfter(today)) && Period.between(date, today).years >= MINIMUM_AGE
        } else {
            val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(birthdate.value!!)

            val bd = Calendar.getInstance()
            bd.time = date!!
            val today = Calendar.getInstance()
            val currentYear = today.get(Calendar.YEAR)
            val currentMonth = today.get(Calendar.MONTH) + 1
            val currentDayOfMonth = today.get(Calendar.DAY_OF_MONTH)

            var age = currentYear - bd.get(Calendar.YEAR)

            if (bd.get(Calendar.MONTH)+1 > currentMonth) {
                --age
            } else if (bd.get(Calendar.MONTH)+1 == currentMonth) {
                if (bd.get(Calendar.DAY_OF_MONTH) > currentDayOfMonth) {
                    --age
                }
            }

            age >= MINIMUM_AGE
        }
    }

    private fun isValidName(): Boolean {
        return !name.value.isNullOrBlank() && name.value?.matches("[a-zA-Z]+( [a-zA-Z]+){1,4}".toRegex())!!
    }

    private fun isValidLocation(): Boolean {
        if (location.value.isNullOrBlank() || markerLat.value == null) {
            return false
        }
        return true
    }

    private fun isValidPhoneNumber(): Boolean {
        return !phonenumber.value.isNullOrBlank() && phonenumber.value?.matches("[0-9]{10,12}".toRegex())!! // todo change regex
    }

    fun setBirthDate(date: String) {
        birthdate.value = date
    }

    fun onRotateImgClockwise() {
        if (_img.value != null) {
            val matrix = Matrix()
            matrix.postRotate(+90.00F)
            val rotatedBitmap = Bitmap.createBitmap(
                _img.value!!, 0, 0, _img.value!!.width, _img.value!!.height,
                matrix, true
            )
            _img.value = rotatedBitmap
            _isImgBeenEdited.value = true
        } else {
            _toastmsg.value = SELECT_IMG
        }
    }

    fun onRotateImgAntiClockwise() {
        if (_img.value != null) {
            val matrix = Matrix()
            matrix.postRotate(-90.00F)
            val rotatedBitmap = Bitmap.createBitmap(
                _img.value!!, 0, 0, _img.value!!.width, _img.value!!.height,
                matrix, true
            )
            _img.value = rotatedBitmap
            _isImgBeenEdited.value = true
        } else {
            _toastmsg.value = SELECT_IMG
        }

    }

    fun setImg(img: Bitmap) {
        var d: Float = img.height.coerceAtMost(img.width).toFloat()
        val scale: Float = (d / 800)
        if (scale != 0F) {
            d /= scale
            val width = img.width / scale
            val height = img.height / scale
            val newImg = Bitmap.createScaledBitmap(img, width.toInt(), height.toInt(), false)
            _img.value = newImg
            _isImgBeenEdited.value = true
        } else {
            _toastmsg.value = SELECT_IMG
        }
    }

    fun setCurrentPhotoPath(path: String) {
        _currentPhotoPath.value = path
    }

    init {
        Log.i(TAG, "$this init")
    }

    override fun onCleared() {
        super.onCleared()
        Log.i(TAG, "onCleared")
    }
}