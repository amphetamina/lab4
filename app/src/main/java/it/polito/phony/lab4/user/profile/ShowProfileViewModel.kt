package it.polito.phony.lab4.user.profile

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.Tasks
import com.google.firebase.storage.StorageException
import it.polito.phony.lab4.user.entity.User
import it.polito.phony.lab4.user.entity.UserRating
import it.polito.phony.lab4.user.repository.UserRepository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.util.concurrent.TimeUnit

class ShowProfileViewModel(id: String?, private val userRepository: UserRepository) : ViewModel() {

    companion object {
        const val TAG = "ShowProfileViewModel"
    }

    private val _user by lazy {
        return@lazy MutableLiveData<User?>(null)
    }
    val user: LiveData<User?>
        get() = _user

    private val _img by lazy {
        return@lazy MutableLiveData<Bitmap?>()
    }
     val img: LiveData<Bitmap?>
        get() = _img

    private val _loadingComplete by lazy {
        return@lazy MutableLiveData(false)
    }
    val loadingComplete: LiveData<Boolean>
        get() = _loadingComplete

    private val _toastmsg by lazy {
        return@lazy MutableLiveData<String?>()
    }
    val toastmsg: LiveData<String?>
        get() = _toastmsg

    fun onShowToastMsgComplete() {
        _toastmsg.value = null
    }

    private val _rating by lazy {
        return@lazy MutableLiveData<UserRating?>(null)
    }
    val rating: LiveData<UserRating?>
        get() = _rating

    fun loadUser(id: String?) {
        viewModelScope.launch(context = IO) {
            id?.let { id ->
                _loadingComplete.postValue(false)
                val data = userRepository.getUserData(id)
                val img = userRepository.getUserImg(id)
                val avg = userRepository.getAvgRating(id)
                _user.postValue(data)
                _img.postValue(img)
                _rating.postValue(avg)
                _loadingComplete.postValue(true)
            }
        }
    }

    init {
        Log.i(TAG, "$this init")
        // loadUser(id)
    }

    fun resetUser() {
        _user.value = null
    }
}
