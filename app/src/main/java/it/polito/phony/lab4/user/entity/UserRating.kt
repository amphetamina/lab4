package it.polito.phony.lab4.user.entity

class UserRating(var hasReviews: Boolean = false, var rating: Float? = null)