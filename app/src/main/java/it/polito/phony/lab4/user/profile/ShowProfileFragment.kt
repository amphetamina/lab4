package it.polito.phony.lab4.user.profile

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentShowProfileBinding
import it.polito.phony.lab4.user.account.AccountViewModel
import it.polito.phony.lab4.user.repository.UserRepository


/**
 * accessible upon user data loading complete
 */
class ShowProfileFragment : Fragment(), OnMapReadyCallback {

    companion object {
        const val TAG = "ShowProfileFragment"
    }

    private lateinit var showProfileViewModel: ShowProfileViewModel
    private lateinit var currentId: String
    private val accountViewModel: AccountViewModel by activityViewModels()

    private lateinit var binding: FragmentShowProfileBinding

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        currentId = arguments?.getString("id")?: accountViewModel.id.value?: "-"

        setHasOptionsMenu(true)
        binding =
            DataBindingUtil.inflate<FragmentShowProfileBinding>(inflater, R.layout.fragment_show_profile, container, false).apply {

                userBirthDate.isGone = true
                userBirthDateLabel.isGone = true
                userPhonenum.isGone = true
                userPhonenumLabel.isGone = true

                accountViewModel.id.value?.let { id ->
                    if (id == currentId) {
                        userBirthDate.isGone = false
                        userBirthDateLabel.isGone = false
                        userPhonenum.isGone = false
                        userPhonenumLabel.isGone = false
                    }
                }
            }


        val userRepository: UserRepository = UserRepository.getInstance(requireActivity())
        showProfileViewModel = ViewModelProvider(this, ShowProfileViewModelFactory(currentId, userRepository)).get(ShowProfileViewModel::class.java)

        binding.showProfileViewModel = showProfileViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        binding.refreshLayout?.setOnRefreshListener {
            showProfileViewModel.loadUser(currentId)
        }

       //MAP FRAGMENT
        val mapFragment : SupportMapFragment = childFragmentManager.findFragmentById(R.id.mainMap) as SupportMapFragment
        mapFragment.getMapAsync(this@ShowProfileFragment)

        //ENABLE VERTICAL SCROLL ON MAP
        val mainScrollView : ScrollView =  binding.root.findViewById(R.id.main_scrollview) as ScrollView
        val transparentImageView : ImageView = binding.root.findViewById(R.id.transparent_image) as ImageView
        transparentImageView.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(true)      // Disallow ScrollView to intercept touch events
                    return@setOnTouchListener false                                             //Disable touch on transparent view
                }
                MotionEvent.ACTION_UP -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(false)     // Allow ScrollView to intercept touch events
                    return@setOnTouchListener true
                }
                MotionEvent.ACTION_MOVE -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(true)
                    return@setOnTouchListener false
                }
                else -> return@setOnTouchListener true
            }
        }

        return binding.root
    }

    override fun onMapReady(myMap: GoogleMap?) {
        try {
            showProfileViewModel.user.observe(viewLifecycleOwner, Observer {
                if (showProfileViewModel.user.value?.markerLat != null && showProfileViewModel.user.value?.markerLong != null) {
                    myMap?.clear()
                    val coordinates = LatLng(
                        showProfileViewModel.user.value?.markerLat!!,
                        showProfileViewModel.user.value?.markerLong!!
                    )
                    myMap?.addMarker(
                        MarkerOptions()
                            .position(coordinates).title("My address")
                    )
                    myMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 16.0f))
                }
            })
        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showProfileViewModel.loadUser(currentId)

        accountViewModel.authenticationState.observe(viewLifecycleOwner, Observer {
            requireActivity().invalidateOptionsMenu()
        })

        showProfileViewModel.loadingComplete.observe(viewLifecycleOwner, Observer { isComplete ->
            if (isComplete) {
                binding.refreshLayout?.isRefreshing = false
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        accountViewModel.user.value?.id?.let { id ->
            if (id == currentId) {
                inflater.inflate(R.menu.show_profile_menu, menu)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.edit_icon -> {
                if (showProfileViewModel.loadingComplete.value!!) {
                    val bundle = bundleOf("user" to showProfileViewModel.user.value)
                    bundle.putParcelable("img", showProfileViewModel.img.value)
                    findNavController().navigate(R.id.action_showProfileFragment_to_editProfileFragment, bundle)
                } else {
                    Toast.makeText(requireContext(), "Please wait the profile to be loaded", Toast.LENGTH_SHORT).show()
                }
                true
            }
            R.id.action_logout -> {
                accountViewModel.logOut()
                findNavController().popBackStack()
                true
            }
            R.id.menu_refresh -> {
                binding.refreshLayout?.isRefreshing = true
                showProfileViewModel.loadUser(currentId)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "$this ~ onCreate")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "$this ~ onDestroy")
    }
}
