package it.polito.phony.lab4.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.DialogRatingBinding
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.notification.Notification
import it.polito.phony.lab4.user.account.AccountViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class RatingDialogFragment(private val ratingListener: RatingListener) : DialogFragment() {
    private var _binding: DialogRatingBinding? = null
    private val binding get() = _binding!!
    private lateinit var notification : Notification
    private lateinit var ratingTransaction: Transaction
    private val accountViewModel: AccountViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notification = Notification(requireActivity())
    }

    interface RatingListener {
        fun onRating(rating: Transaction)
    }

    fun newInstance(t: Transaction): RatingDialogFragment {
        val args = Bundle()
        args.putSerializable(TRANSACTION, t)
        val fragment = (this)
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogRatingBinding.inflate(inflater, container, false)

        arguments?.let{
            ratingTransaction = it.get(TRANSACTION) as Transaction
            binding.transaction = ratingTransaction
        }

        binding.raitingFormButton.setOnClickListener { onSubmitClicked() }
        binding.ratingFormCancel.setOnClickListener { onCancelClicked() }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun onSubmitClicked() {
        val user = accountViewModel.user.value!!.id
        if (user != ratingTransaction.buyerID) return
        user.let {
            val rating = Transaction(
                ratingTransaction.transID,
                true,
                binding.userRatingBar.rating,
                binding.userRatingFormText.text.toString())

            ratingListener.onRating(rating)
        }

        //SEND ADV REVIEWED NOTIFICATION
        val advId = ratingTransaction.advID
        MainScope().launch(context = Dispatchers.IO) {
            val advItem = AdvertisementRepository.getInstance(requireActivity()).getAdvertisemntByIdAwait(advId)
            val advTitle = advItem?.title
            notification.send("/topics/advReviewed$advId",
                "Item reviewed",
                "You have received a new review for the item $advTitle",
                "advertisement_id",
                advId,
                this.toString(),
                R.id.itemDetailsFragment)
        }

        dismiss()
    }

    private fun onCancelClicked() {
        dismiss()
    }

    companion object {

        const val TAG = "RatingDialog"
        const val TRANSACTION = "TRANSACTION"
    }
}