package it.polito.phony.lab4.item

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.Tasks
import it.polito.phony.lab4.R
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.itemlist.ItemListFragment.AdvertisementKey.IMAGE_ID_KEY
import it.polito.phony.lab4.user.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch


class ItemImageFragment : Fragment() {

    private lateinit var imageId : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            imageId = it.get(IMAGE_ID_KEY).toString()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_item_image, container, false)
        val advertisementImage = view.findViewById<ImageView>(R.id.itemImage)
        loadImage(imageId, advertisementImage)

        return view
    }

    fun loadImage(imageId: String?, imageView: ImageView?) {
        try{
            val imageref = imageId?.let {
                AdvertisementRepository.getInstance(requireActivity()).getAdvertisementImg(
                    it
                )
            }
            if (imageView != null) {
                Glide.with(requireContext())
                    .load(imageref)
                    .into(imageView)
            }
        } catch(ex: Throwable) {
            Log.i("TAG", "$imageId img not found")
        }

        /*if (imageId != null) {
            GlobalScope.launch(context = Dispatchers.IO) {
                try {
                    val imgTask = AdvertisementRepository.getInstance(requireActivity()).getAdvertisementImg(imageId).stream
                    Tasks.await(imgTask).let {
                        imageView?.setImageBitmap(BitmapFactory.decodeStream(it.stream))
                    }
                } catch (ex: Throwable) {
                    Log.i("ItemViewModel", "$imageId img loading fail")
                }
            }
        }*/
    }
}