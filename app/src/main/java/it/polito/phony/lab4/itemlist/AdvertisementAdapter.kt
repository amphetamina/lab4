package it.polito.phony.lab4.itemlist

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import it.polito.phony.lab4.databinding.AdvertisementItemBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import java.util.*

open class AdvertisementAdapter(
    private val clickListener: onAdvertisementClickListener,
    private val editClickListener: onEditClickListener,
    private val deleteClickListener: onDeleteClickListener,
    private val advertisementRepository: AdvertisementRepository,
    options: FirestoreRecyclerOptions<Advertisement>,
    val listViewModel: ItemListViewModel)
    : FirestoreRecyclerAdapter<Advertisement, AdvertisementAdapter.ViewHolder>(options) {

    interface onAdvertisementClickListener{
        fun advertisementClickHandler(item: Advertisement)
    }

    interface onEditClickListener{
        fun editClickHandler(item: Advertisement)
    }

    interface onDeleteClickListener{
        fun deleteClickHandler(item: Advertisement)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            AdvertisementItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    class ViewHolder(val binding:AdvertisementItemBinding): RecyclerView.ViewHolder(binding.root) {

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, item: Advertisement) {
        holder.apply{
            if (item.id.isBlank() || (item.status == Advertisement.Status.Sold.toString()) || (item.expirationdate!! < Date())){
                binding.itemCard.visibility = View.GONE
            }
            else{
                binding.itemCard.visibility = View.VISIBLE
                binding.item = item
                binding.listViewModel = listViewModel
                binding.itemTitle.text = item.title
                binding.itemLocation.text = item.location
                binding.itemPrice.text = item.getPriceStr()
                binding.interestUsersCount.text = if (item.interestedUsers != null) item.interestedUsers.size.toString() else "0"

                //LOAD IMAGE
                val id = item.id
                try {
                    val imgRef = advertisementRepository.getAdvertisementImg(id)
                    Glide.with(binding.itemImage.context).load(imgRef).centerInside().into(binding.itemImage)
                } catch (ex: Throwable) {
                    Log.i("AdvertisementAdapter", "$id img not found")
                }

                //ADVERTISEMENT CARD LISTENER
                binding.itemCard.setOnClickListener{
                    clickListener.advertisementClickHandler(item)
                }

                if(listViewModel.isOwner){
                    //EDIT BUTTON LISTENER
                    binding.editAdvertisement.setOnClickListener{
                        editClickListener.editClickHandler(item)
                    }

                    //DELETE BUTTON LISTENER
                    binding.deleteAdvertisement.setOnClickListener{
                        deleteClickListener.deleteClickHandler(item)
                    }
                }
                else{
                    binding.editAdvertisement.visibility = View.GONE
                    binding.deleteAdvertisement.visibility = View.GONE
                }
            }
        }
    }
}