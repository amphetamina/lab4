package it.polito.phony.lab4.user.account

import android.accounts.Account
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.AccountFragmentBinding
import it.polito.phony.lab4.user.entity.User


class AccountFragment : Fragment() {

    companion object {
        const val TAG = "AccountFragment"
        const val SIGN_IN_RESULT_CODE = 1846
    }

    private lateinit var binding: AccountFragmentBinding
    private lateinit var navController: NavController
    private val accountViewModel: AccountViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(inflater, R.layout.account_fragment, container, false)

        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = findNavController()

        accountViewModel.authenticationState.observe(viewLifecycleOwner, Observer { authState ->
            requireActivity().invalidateOptionsMenu()
            when (authState) {
                AccountViewModel.AuthenticationState.AUTHENTICATED -> {
                    if (accountViewModel.user.value == null) {
                        binding.mainMsg.setText(R.string.complete_profile)
                        binding.authButton.textAlignment = View.TEXT_ALIGNMENT_CENTER
                        binding.authButton.setText(R.string.complete_profile_button)
                        binding.authButton.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null)
                        binding.authButton.setOnClickListener {
                            val bundle = bundleOf("user" to User(id = accountViewModel.id.value))
                            // bundle.putParcelable("img", accountViewModel.img.value)
                            navController.navigate(R.id.action_nav_login_to_nav_edit_profile, bundle)
                        }
                    } else {
                        binding.mainMsg.text = "Welcome "+ accountViewModel.user.value!!.name+"!"
                        binding.authButton.isVisible = false
                        Snackbar.make(binding.root, "User correctly authenticated", Snackbar.LENGTH_SHORT).show()
                        navController.navigate(R.id.action_nav_login_to_nav_OnSaleListFragment)
                    }
                }
                AccountViewModel.AuthenticationState.UNAUTHENTICATED -> {
                    binding.authButton.setOnClickListener {
                        if (accountViewModel.authenticationState.value != AccountViewModel.AuthenticationState.LOADING) {
                            launchSignInFlow()
                        } else {
                            Toast.makeText(view.context, "Currently authenticating", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                else -> return@Observer
            }
        })
    }

    private fun launchSignInFlow() {

        val providers = arrayListOf(
            AuthUI.IdpConfig.GoogleBuilder().build()
        )

        startActivityForResult(
            AuthUI.getInstance().createSignInIntentBuilder().setAvailableProviders(
                providers
            ).build(), SIGN_IN_RESULT_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SIGN_IN_RESULT_CODE) {
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == Activity.RESULT_OK) {
                Log.i(
                    TAG,
                    "Successfully signed in user " +
                            "${FirebaseAuth.getInstance().currentUser?.displayName}!"
                )
            } else {

                Log.i(TAG, "Sign in unsuccessful ${response?.error?.errorCode}")
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (accountViewModel.authenticationState.value == AccountViewModel.AuthenticationState.AUTHENTICATED) {
            inflater.inflate(R.menu.account_menu, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_logout -> {
                accountViewModel.logOut()
                findNavController().popBackStack()
                true
            }
             else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "$this ~ onCreate")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "$this ~ onDestroy")
    }

}
