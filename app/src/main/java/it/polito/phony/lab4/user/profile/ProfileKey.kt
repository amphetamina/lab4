package it.polito.phony.lab4.user.profile

object ProfileKey {
    const val REQUEST_TAKE_PHOTO = 1
    const val SELECT_IMAGE = 2
}