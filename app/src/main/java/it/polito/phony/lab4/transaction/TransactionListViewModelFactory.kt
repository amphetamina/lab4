package it.polito.phony.lab4.transaction


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class TransactionListViewModelFactory(private val isSeller: Boolean): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(TransactionListViewModel::class.java)) {
            return TransactionListViewModel(isSeller) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}