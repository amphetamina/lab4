package it.polito.phony.lab4.entity


import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.ServerTimestamp
import java.util.*
import java.io.Serializable

@IgnoreExtraProperties
data class Advertisement (
    @Exclude var id: String = "",
    val userid: Any? = null,
    val title: String = "",
    val description: String = "",
    val location: String = "",
    val price: Double = 0.0,
    val category: String = "",
    val subcategory: String = "",
    @ServerTimestamp val expirationdate: Date? = null,
    @ServerTimestamp val creationdate: Date? = null,
    val status: String = "",
    val img: String = "",
    val interestedUsers : List<String>? = null,
    val markerLat : Double? = null,
    val markerLong : Double? = null
): Serializable {
    fun withDocumentKey(key: String) {
        this.id = key
    }

    fun getPriceStr(): String {
        return "${price.toInt()} €"
    }

    companion object {
        const val FIELD_CATEGORY = "category"
        const val FIELD_PRICE = "price"
        const val FIELD_CREATION_DATE = "creationdate"
        const val FIELD_EXPIRATION_DATE = "expirationdate"
        const val FIELD_STATUS = "status"
        const val FIELD_USER = "userid"
        const val FIELD_INTERESTED_USERS = "interestedUsers"
    }

    enum class Status {
        Available, Sold, Blocked
    }
}