package it.polito.phony.lab4.transaction
import androidx.lifecycle.ViewModel

class TransactionListViewModel(_isSeller: Boolean) : ViewModel() {
    var isSeller: Boolean = _isSeller
    var currentUserID : String? = null

}