package it.polito.phony.lab4.itemlist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.Query

import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentFilterDialogBinding
import it.polito.phony.lab4.entity.Advertisement

class FilterDialogFragment(private val filterListener: FilterListener) : DialogFragment() {

    private var _binding: FragmentFilterDialogBinding? = null
    private val binding get() = _binding!!
    private lateinit var listViewModel: ItemListViewModel
    private lateinit var listViewModelFactory: ItemListViewModelFactory

    private val selectedCategory: String?
        get() {
            val selected = binding.spinnerCategory.selectedItem as String
            return if (getString(R.string.any_category) == selected) {
                null
            } else {
                selected
            }
        }

    private val searchTitle: String?
        get(){
            val searchMatch = binding.itemTitleSearch.text.toString()
            if (searchMatch.isNullOrEmpty())
                return null
            return searchMatch
        }

    private val selectedSortBy: String?
        get() {

            val selected = binding.spinnerSort.selectedItem as String
            if (getString(R.string.sort_by_creation_date) == selected) {
                return Advertisement.FIELD_CREATION_DATE
            }
            if (getString(R.string.sort_by_price_asc) == selected) {
                return Advertisement.FIELD_PRICE
            }
            if (getString(R.string.sort_by_price_desc) == selected) {
                return Advertisement.FIELD_PRICE
            } else {
                return null
            }
        }

    private val sortDirection: Query.Direction
        get() {
            val selected = binding.spinnerSort.selectedItem as String
            if (getString(R.string.sort_by_creation_date) == selected) {
                return Query.Direction.DESCENDING
            }
            if (getString(R.string.sort_by_price_asc) == selected) {
                return Query.Direction.ASCENDING
            }
            if (getString(R.string.sort_by_price_desc) == selected) {
                return Query.Direction.DESCENDING
            } else {
                return Query.Direction.DESCENDING
            }
        }

    val filters: Filters
        get() {
            val filters = Filters()

            filters.category = selectedCategory
            filters.text = searchTitle
            filters.sortBy = selectedSortBy
            filters.sortDirection = sortDirection

            return filters
        }


    interface FilterListener {
        fun onFilter(filters: Filters)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFilterDialogBinding.inflate(inflater, container, false)

        // View model
        listViewModelFactory = ItemListViewModelFactory(false)
        listViewModel = ViewModelProvider(this, listViewModelFactory).get(ItemListViewModel::class.java)
        listViewModel.filters = filters
        binding.listViewModel = listViewModel

        binding.buttonSearch.setOnClickListener { onSearchClicked() }
        binding.buttonCancel.setOnClickListener { onCancelClicked() }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun onSearchClicked() {
        listViewModel.filters = filters
        filterListener.onFilter(filters)
        dismiss()
    }

    private fun onCancelClicked() {
        dismiss()
    }

    fun resetFilters() {
        binding.let {
            it.spinnerCategory.setSelection(0)
            it.itemTitleSearch.text.clear()
            it.spinnerSort.setSelection(0)
        }
    }

    companion object {
        const val TAG = "FilterDialog"
    }
}
