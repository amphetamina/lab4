package it.polito.phony.lab4.itemlist

import android.content.Context
import android.text.TextUtils
import com.google.firebase.firestore.Query
import it.polito.phony.lab4.R
import it.polito.phony.lab4.entity.Advertisement
import java.io.Serializable

class Filters : Serializable {

    var text: String? = null
    var category: String? = null

    var sortBy: String? = null
    var sortDirection: Query.Direction = Query.Direction.DESCENDING

    fun hasText(): Boolean {
        return !TextUtils.isEmpty(text)
    }

    fun hasCategory(): Boolean {
        return !TextUtils.isEmpty(category)
    }

    fun hasSortBy(): Boolean {
        return !TextUtils.isEmpty(sortBy)
    }

    fun getSearchDescription(context: Context): String {
        val desc = StringBuilder()

        if (category == null && text == null) {
            desc.append("<b>")
            desc.append(context.getString(R.string.all_advertisements))
            desc.append("</b>")
        }

        if (text != null){
            desc.append("<b>")
            desc.append(text)
            desc.append("</b>")
        }

        if (category != null) {
            desc.append(" in ")
        }

        if (category != null) {
            desc.append("<b>")
            desc.append(category)
            desc.append("</b>")
        }

        return desc.toString()
    }

    fun getOrderDescription(context: Context): String {
        return when (sortBy) {
            Advertisement.FIELD_PRICE -> context.getString(R.string.sorted_by_price)
            else -> context.getString(R.string.sorted_by_creation_date)
        }
    }

    companion object {

        val default: Filters
            get() {
                val filters = Filters()
                filters.sortBy = Advertisement.FIELD_CREATION_DATE
                filters.sortDirection = Query.Direction.DESCENDING

                return filters
            }
    }
}