package it.polito.phony.lab4.request

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.collection.ArraySet
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentShowRequestsBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.notification.Notification
import it.polito.phony.lab4.transaction.Transaction
import it.polito.phony.lab4.transaction.TransactionRepository
import it.polito.phony.lab4.user.account.AccountViewModel
import it.polito.phony.lab4.user.repository.UserRepository
import java.util.*
import kotlin.collections.ArrayList


class ShowRequestsFragment : Fragment(),
    RequestAdapter.OnSetBuyerClickListener {

    companion object RequestKey {
        const val ADVERTISEMENT = "group39.lab4.ADVERTISEMENT"
        const val INTERESTED_USERS = "group39.lab4.INTERESTED_USERS"
    }

    private var advertisement: Advertisement? = null
    private var interestedUsers: ArrayList<String>? = null
    private var requests: ArraySet<Request> = ArraySet()
    private var isNotAvailable : Boolean? = null
    private val accountViewModel: AccountViewModel by activityViewModels()
    private lateinit var binding: FragmentShowRequestsBinding
    private lateinit var tr : TransactionRepository
    private lateinit var ur : UserRepository
    private lateinit var ar : AdvertisementRepository
    private lateinit var notification : Notification

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            advertisement = it.get(ADVERTISEMENT) as Advertisement
            interestedUsers = it.getStringArrayList(INTERESTED_USERS)
            notification = Notification(requireActivity())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        requests.clear()

        binding = FragmentShowRequestsBinding.inflate(layoutInflater)
        binding.lifecycleOwner = viewLifecycleOwner
        //getting repos instances
        ur = UserRepository.getInstance(this.requireActivity())
        tr = TransactionRepository.getInstance(this.requireActivity(), ur)
        ar = AdvertisementRepository.getInstance(requireActivity())

        isNotAvailable = advertisement!!.status != "Available"

        if(!interestedUsers.isNullOrEmpty()) {
            binding.requestsText.text = getString(R.string.requests)

            for (u in interestedUsers!!) {
                requests.add(Request(u, false))
            }
        }else{
            binding.requestsText.text = getString(R.string.no_requests)
        }

        binding.requestsRecyclerView.adapter = RequestAdapter(requests, isNotAvailable!!,
            this.requireActivity(), this::onRequestClickHandler, this)

        // check if there are already transactions for this advertisement
        tr.getTransactionByAdvId(advertisement!!.id).get().addOnSuccessListener { querySnapshot ->
            if(!querySnapshot.isEmpty){
                val transactions = querySnapshot.toObjects(Transaction::class.java)
                val buyerId = transactions[0].buyerID
                var idx : Int? = null
                var cnt = 0
                requests.forEach {
                    if (it.userId == buyerId){
                        it.isBuyer = true
                        isNotAvailable = true
                        idx = cnt
                    }
                    cnt += 1
                }
                if(idx != null)
                    binding.requestsRecyclerView.adapter = RequestAdapter(requests, isNotAvailable!!,
                        this@ShowRequestsFragment.requireActivity(),
                        this@ShowRequestsFragment::onRequestClickHandler,
                        this@ShowRequestsFragment)
            }
        }

        binding.requestsRecyclerView.layoutManager = LinearLayoutManager(context)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
    }

    //listener click on a request
    private fun onRequestClickHandler(position:Int){
        val b = bundleOf(
            "id" to requests.valueAt(position)!!.userId)
        findNavController().navigate(R.id.action_showRequestsFragment_to_nav_show_profile, b)
    }

    //listener click on setBuyerBtn
    override fun setBuyerClickHandler(r: Request) {

        val dialogBuilder = AlertDialog.Builder(activity)
        dialogBuilder.setMessage("Do you want to set ${r.userId} as the buyer?")
            .setCancelable(false)
            .setNegativeButton("No") {dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton("Yes"){dialog, _ ->
                dialog.dismiss()
                var positiveText = "Buyer set"
                if (accountViewModel.user.value?.id != null) {
                    val sellerID: String? = accountViewModel.user.value!!.id
                    val buyerID = r.userId
                    val advID = advertisement!!.id
                    val totalPrice = advertisement!!.price.toString() + "€"

                    val transaction = Transaction(UUID.randomUUID().toString(),
                        sellerID!!, buyerID, advID,
                        advertisement!!.title, totalPrice)
                    //write into db
                    tr.addTransaction(transaction)
                    ar.updateAdvertisementStatus(advID, Advertisement.Status.Sold)

                    isNotAvailable = true
                    requests.forEach{
                        if(it.userId == r.userId){
                            r.isBuyer = true
                        }
                    }
                    binding.requestsRecyclerView.adapter = RequestAdapter(requests, isNotAvailable!!,
                        this.requireActivity(), this::onRequestClickHandler, this)

                    ////TODO remember to add rating + comment from the ShowTransactionFragment (each user has his BoughtItemList -> can rate, and his SoldItemList -> can't rate)

                    //SEND ADV SOLD NOTIFICATION TO ALL THE INTERESTED USERS (INCLUDED THE ONE TO WHICH IT HAS BEEN SOLD)
                    val advTitle = advertisement!!.title
                    notification.send("/topics/advSold$advID",
                        "Item sold",
                        "The item $advTitle has been sold",
                        "advertisement_id",
                        advID,
                        this.toString(),
                        R.id.itemDetailsFragment)

                    //SUBSCRIBE TO ADVERTISEMENT SOLD TOPIC
                    notification.subscribe("/topics/advReviewed$advID", this.toString())

                }else{
                    positiveText = "Please log in first"
                }

                val snacky = Snackbar.make(
                    requireActivity().findViewById(R.id.coordinatorLayout),
                    positiveText,
                    Snackbar.LENGTH_LONG
                )

                snacky.show()

            }
        val alert = dialogBuilder.create()
        alert.show()

        // TODO nav controller redirect to user advertisement
    }
}