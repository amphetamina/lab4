package it.polito.phony.lab4.itemlist

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.firebase.ui.firestore.SnapshotParser
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentItemListBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.notification.Notification
import it.polito.phony.lab4.user.account.AccountViewModel

class ItemListFragment : Fragment(),
    AdvertisementAdapter.onAdvertisementClickListener,
    AdvertisementAdapter.onEditClickListener,
    AdvertisementAdapter.onDeleteClickListener {

    private lateinit var advertisementRepository : AdvertisementRepository
    lateinit var query: Query
    lateinit var options: FirestoreRecyclerOptions<Advertisement>
    lateinit var adapter : AdvertisementAdapter
    private lateinit var binding: FragmentItemListBinding
    private lateinit var listViewModel: ItemListViewModel
    private lateinit var listViewModelFactory: ItemListViewModelFactory
    private val accountViewModel: AccountViewModel by activityViewModels()
    private lateinit var notification : Notification

    companion object AdvertisementKey {
        const val ID_KEY = "group39.lab4.ID"
        const val ADV_PHOTO_PREFIX = "group39.lab4.advertisementImage"
        const val MARKER_KEY = "group39.lab4.MARKER"
        const val LOCATION_KEY = "group39.lab4.LOCATION"
        const val LATITUDE_KEY = "group39.lab4.LATITUDE"
        const val LONGITUDE_KEY = "group39.lab4.LONGITUDE"
        const val PROFILE_LOCATION_KEY = "group39.lab4.PROFILE_LOCATION"
        const val PROFILE_LATITUDE_KEY = "group39.lab4.PROFILE_LATITUDE"
        const val PROFILE_LONGITUDE_KEY = "group39.lab4.PROFILE_LONGITUDE"
        const val IMAGE_ID_KEY = "group39.lab4.IMAGE_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notification = Notification(requireActivity())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("kkk","$this onCreateView")

        setHasOptionsMenu(true)
        advertisementRepository = AdvertisementRepository.getInstance(requireActivity())
        binding = FragmentItemListBinding.inflate(layoutInflater)
        binding.lifecycleOwner = viewLifecycleOwner

        // View model
        listViewModelFactory = ItemListViewModelFactory(true)
        listViewModel = ViewModelProvider(this, listViewModelFactory).get(ItemListViewModel::class.java)

        if(accountViewModel.authenticationState.value == AccountViewModel.AuthenticationState.AUTHENTICATED
            && accountViewModel.user.value != null){
            listViewModel.currentUserId = accountViewModel.user.value!!.id!!
        }
        else{
            listViewModel.currentUserId = "-"
        }

        binding.viewModel = listViewModel
        query = advertisementRepository.getUserAdvertisements(listViewModel.currentUserId!!)
        options = FirestoreRecyclerOptions.Builder<Advertisement>().setQuery(query
        ) { snapshot ->
            snapshot.toObject(Advertisement::class.java)!!.also {
                it.id = snapshot.id
            }
        }.build()

        adapter = object : AdvertisementAdapter(this@ItemListFragment,
            this@ItemListFragment, this@ItemListFragment, advertisementRepository, options, listViewModel){
            override fun onDataChanged() {
                // Show/hide content if the query returns empty.
                if (itemCount == 0) {
                    binding.recyclerView.visibility = View.GONE
                    binding.feedback.visibility = View.VISIBLE
                } else {
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.feedback.visibility = View.GONE
                }
            }

            override fun onError(e: FirebaseFirestoreException) {
                // Show a snackbar on errors
                Snackbar.make(binding.root,
                    "Error: check logs for info.", Snackbar.LENGTH_LONG).show()
                Log.d("ERROR-ItemListFragment", e.toString())
            }
        }

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)

        //FAB listener (+ button)
        val addAdvertisement: FloatingActionButton = binding.root.findViewById(R.id.add_advertisement)
        addAdvertisement.setOnClickListener {
            val b = bundleOf("Action" to "Add")
            findNavController().navigate(R.id.action_itemListFragment_to_itemEditFragment2,b)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = findNavController()
        accountViewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when (authenticationState) {
                AccountViewModel.AuthenticationState.UNAUTHENTICATED -> navController.navigate(R.id.nav_sign_in)
                AccountViewModel.AuthenticationState.AUTHENTICATED ->
                    if(accountViewModel.user.value != null) {
                        listViewModel.currentUserId = accountViewModel.user.value!!.id!!
                        binding.viewModel = listViewModel
                        query =
                            advertisementRepository.getUserAdvertisements(listViewModel.currentUserId!!)
                        options = FirestoreRecyclerOptions.Builder<Advertisement>().setQuery(query
                        ) { snapshot ->
                            snapshot.toObject(Advertisement::class.java)!!.also {
                                it.id = snapshot.id
                            }
                        }.build()
                        adapter.updateOptions(options)
                        binding.recyclerView.adapter = adapter
                    }
            }
        })
    }

    override fun onStart() {
        super.onStart()

        // Start listening for Firestore updates
        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //CLOSE KEYBOARD if opened
        val view = requireActivity().currentFocus
        if (view != null) {
            val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(requireView().windowToken, 0)
        }
    }

    //listener click on an advertisement
    override fun advertisementClickHandler(item: Advertisement) {
        val b = Bundle()
        b.putSerializable("advertisement", item)
        findNavController().navigate(R.id.action_itemListFragment_to_itemDetailsFragment, b)
    }

    //listener click on edit
    override fun editClickHandler(item: Advertisement){
        val b = bundleOf("Action" to "Edit")
        b.putSerializable("advertisement", item)
        findNavController().navigate(R.id.action_itemListFragment_to_itemEditFragment2,b)
    }

    //listener click on delete
    override fun deleteClickHandler(item: Advertisement){
        val dialogBuilder = AlertDialog.Builder(activity)
        dialogBuilder.setMessage("Do you want to delete this advertisement ?")
            // if the dialog is cancelable
            .setCancelable(false)
            .setNegativeButton("No") { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton("Yes") { dialog, _ ->
                dialog.dismiss()
                // delete adv
                advertisementRepository.removeAdvertisement(accountViewModel.user.value!!.id!!, item.id)
                val mySnackbar = Snackbar.make(
                    requireActivity().findViewById(R.id.coordinatorLayout),
                    "Advertisement deleted",
                    Snackbar.LENGTH_LONG
                )
                mySnackbar.show()

                //SEND ADV DELETED NOTIFICATION
                val advId = item.id
                val advTitle = item.title
                notification.send("/topics/advDeleted$advId",
                    "Advertisement deleted",
                    "The advertisement $advTitle has been deleted",
                    "",
                    "",
                    this.toString(),
                    R.id.itemsOfInterestListFragment)

                //UNSUBSCRIBE TO INTERESTED USERS TOPIC
                notification.unsubscribe("/topics/newInterestedUsers$advId", this.toString())
            }
        val alert = dialogBuilder.create()
        alert.show()
    }
}