package it.polito.phony.lab4.request

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.collection.ArraySet
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.card.MaterialCardView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.RequestItemBinding
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.user.repository.UserRepository


class RequestAdapter(val requests: ArraySet<Request>,
                     val isNotAvailable: Boolean,
                     val activity: Activity,
                     val requestClickHandler: (Int) -> Unit,
                     val buyerBtnClickListener: OnSetBuyerClickListener)
    : RecyclerView.Adapter<RequestAdapter.ViewHolder>(){

    private var adapterRequests : ArraySet<Request> = requests

    /**
     * THIS ADAPTER HAS INTERFACES THAT HAVE TO BE OVERRIDDEN TO IMPLEMENT
     */

    interface  OnSetBuyerClickListener {
        fun setBuyerClickHandler(r: Request)
    }

    class ViewHolder(val binding : RequestItemBinding): RecyclerView.ViewHolder(binding.root){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        Log.d("kkk","$this onCreateViewHolder")
        return ViewHolder(RequestItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ))
    }

    override fun getItemCount(): Int {
        return adapterRequests.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d("kkk","$this onBindViewHolder")
        adapterRequests.valueAt(position)?.let {
            holder.apply{
                binding.item = it
                binding.userId.text = it.userId

                //LOAD IMAGE
                val userID = it.userId
                try {
                    val imageref = UserRepository.getInstance(activity).getUserImgReference(userID)

                    Glide.with(binding.userImg.context)
                        .load(imageref)
                        .into(binding.userImg)

                } catch(ex: Throwable) {
                    Log.i("TAG", "$userID img not found")
                }

                //REQUEST CARD LISTENER
                binding.itemCard.setOnClickListener{
                    requestClickHandler.invoke(holder.adapterPosition)
                }

                //SET BUYER BTN
                if(isNotAvailable){
                    if(it.isBuyer){
                        binding.buyerBtn.setImageDrawable(binding.root.context.getDrawable(R.drawable.save_icon_primary_color))
                    }
                    else{
                        binding.buyerBtn.visibility = View.GONE
                    }
                }
                else{
                    binding.buyerBtn.visibility = View.VISIBLE
                    //SET BUYER BTN LISTENER
                    binding.buyerBtn.setOnClickListener {
                        buyerBtnClickListener.setBuyerClickHandler(requests.valueAt(position)!!)
                    }
                }
            }
        }
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        //UNBIND
        holder.binding.itemCard.setOnClickListener(null)
        holder.binding.buyerBtn.setOnClickListener(null)
    }
}