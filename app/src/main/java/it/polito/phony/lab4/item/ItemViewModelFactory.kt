package it.polito.phony.lab4.item

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository

class ItemViewModelFactory(private val item: Advertisement, private val img: Bitmap, private val currentUserId: String, private val advertisementRepository: AdvertisementRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ItemViewModel::class.java)) {
            return ItemViewModel(item, img, currentUserId, advertisementRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}