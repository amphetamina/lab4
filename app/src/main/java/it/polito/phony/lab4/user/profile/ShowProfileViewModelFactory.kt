package it.polito.phony.lab4.user.profile

import android.graphics.Bitmap
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import it.polito.phony.lab4.user.repository.UserRepository

class ShowProfileViewModelFactory(private val id: String, private val userRepository: UserRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ShowProfileViewModel::class.java)) {
            return ShowProfileViewModel(id, userRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}