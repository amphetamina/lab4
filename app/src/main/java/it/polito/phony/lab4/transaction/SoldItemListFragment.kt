package it.polito.phony.lab4.transaction

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.collection.ArraySet
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.firebase.ui.firestore.SnapshotParser
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentShowTransactionsBuyerBinding
import it.polito.phony.lab4.databinding.FragmentShowTransactionsSellerBinding
import it.polito.phony.lab4.databinding.TransactionItemBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.transaction.TransactionRepository
import it.polito.phony.lab4.user.account.AccountViewModel
import it.polito.phony.lab4.user.repository.UserRepository

class SoldItemListFragment : Fragment(),
    SoldItemAdapter.OnTransactionClickListener {

    companion object AdvertisementKey {
        const val ID_KEY = "group39.lab4.ID" ///TODO define it into TransactionDetail
    }

    private lateinit var tr : TransactionRepository
    private lateinit var ur : UserRepository
    private lateinit var ar : AdvertisementRepository
    private val accountViewModel: AccountViewModel by activityViewModels()
    private lateinit var binding: FragmentShowTransactionsSellerBinding
    private lateinit var listViewModel: TransactionListViewModel
    private lateinit var listViewModelFactory: TransactionListViewModelFactory
    lateinit var query : Query
    lateinit var adapter : SoldItemAdapter
    lateinit var options : FirestoreRecyclerOptions<Transaction>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //I'm navigating here from the sidebar menu, I can pass the user id
        arguments?.let {
            ////TODO FILL WITH PASSED ARGUMENTS from sidebar menu
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)

        //ViewModel, passing true because I want to show here user items as the buyer
        listViewModelFactory = TransactionListViewModelFactory(true)
        listViewModel = ViewModelProvider(this, listViewModelFactory).get(TransactionListViewModel::class.java)

        if(accountViewModel.authenticationState.value == AccountViewModel.AuthenticationState.AUTHENTICATED
            && accountViewModel.user.value != null){
            listViewModel.currentUserID = accountViewModel.user.value!!.id!!
        }
        else{
            listViewModel.currentUserID = "-"
        }

        ar = AdvertisementRepository.getInstance(requireActivity())
        ur = UserRepository.getInstance(requireActivity())
        tr = TransactionRepository.getInstance(requireActivity(), ur)

        binding = FragmentShowTransactionsSellerBinding.inflate(layoutInflater)
        binding.lifecycleOwner = viewLifecycleOwner

        binding.viewModel = listViewModel

        query = tr.getTransactionsBySellerID(listViewModel.currentUserID!!)

        options = FirestoreRecyclerOptions.Builder<Transaction>().setQuery(query
        ) { snap ->
            snap.toObject(Transaction::class.java)!!.also {
                it.transID = snap.id
            }
        }.build()

        adapter = object : SoldItemAdapter(this@SoldItemListFragment,
             requireActivity(), options, listViewModel) {

            override fun onDataChanged() {
                if(itemCount == 0) {
                    binding.transactionsSellerRecyclerView.visibility = View.GONE
                    binding.transactionsSellerText.visibility = View.VISIBLE
                } else{
                    binding.transactionsSellerRecyclerView.visibility = View.VISIBLE
                    binding.transactionsSellerText.visibility = View.GONE
                }
            }

            override fun onError(e: FirebaseFirestoreException) {
                Snackbar.make(binding.root,
                    "Error: check logs for info.", Snackbar.LENGTH_LONG).show()
                Log.d("ERR-SoldItemListFrag", e.toString())
            }
        }

        binding.transactionsSellerRecyclerView.adapter = adapter
        binding.transactionsSellerRecyclerView.layoutManager = LinearLayoutManager(context)


        return binding.root
    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val view = requireActivity().currentFocus
        if(view != null) {
            val imm: InputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(requireView().windowToken, 0)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = findNavController()
        accountViewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when (authenticationState) {
                AccountViewModel.AuthenticationState.UNAUTHENTICATED -> navController.navigate(R.id.nav_sign_in)
                AccountViewModel.AuthenticationState.AUTHENTICATED ->
                    if(accountViewModel.user.value != null) {
                        query = tr.getTransactionsBySellerID(accountViewModel.user.value!!.id!!)
                        options = FirestoreRecyclerOptions.Builder<Transaction>().setQuery(query
                        ) { snapshot ->
                            snapshot.toObject(Transaction::class.java)!!
                                .also {
                                    it.transID = snapshot.id
                                }
                        }.build()
                        adapter.updateOptions(options)
                        binding.transactionsSellerRecyclerView.adapter = adapter
                    }
            }
        })
    }

    override fun transactionsClickHandler(t: Transaction) {
        val b = Bundle()
        ar.getAdvertisementById(t.advID).get().addOnSuccessListener {
            if(it != null){
                val advObj = it.toObject(Advertisement::class.java)
                if (advObj != null) {
                    advObj.id = it.id
                    b.putSerializable("advertisement", advObj)
                    findNavController().navigate(R.id.action_soldItemsListFragment_to_itemDetailsFragment, b)
                }
            }
            else{
                val mySnackbar = Snackbar.make(
                    requireActivity().findViewById(R.id.coordinatorLayout),
                    "Advertisement was not found",
                    Snackbar.LENGTH_LONG
                )
                mySnackbar.show()
            }
        }.addOnFailureListener {
            val mySnackbar = Snackbar.make(
                requireActivity().findViewById(R.id.coordinatorLayout),
                "Advertisement was not found",
                Snackbar.LENGTH_LONG
            )
            mySnackbar.show()
        }
    }
}
