package it.polito.phony.lab4.itemlist

import android.content.Context
import android.os.Bundle
import android.transition.Slide
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentFilterDialogBinding
import it.polito.phony.lab4.databinding.FragmentItemOnSaleListBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.user.account.AccountViewModel
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator


class OnSaleListFragment : Fragment(),
    AdvertisementAdapter.onAdvertisementClickListener,
    AdvertisementAdapter.onEditClickListener,
    AdvertisementAdapter.onDeleteClickListener,
    FilterDialogFragment.FilterListener {

    private val accountViewModel: AccountViewModel by activityViewModels()
    lateinit var adapter : AdvertisementAdapter
    lateinit var query: Query
    lateinit var options: FirestoreRecyclerOptions<Advertisement>
    private lateinit var advertisementRepository: AdvertisementRepository
    private lateinit var binding: FragmentItemOnSaleListBinding
    private lateinit var filterBinding : FragmentFilterDialogBinding
    private lateinit var filterDialog: FilterDialogFragment
    private lateinit var listViewModel: ItemListViewModel
    private lateinit var listViewModelFactory : ItemListViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        advertisementRepository = AdvertisementRepository.getInstance(requireActivity())
        binding = FragmentItemOnSaleListBinding.inflate(layoutInflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.recyclerViewOnSale.layoutManager = LinearLayoutManager(context)

        // View model
        listViewModelFactory = ItemListViewModelFactory(false)
        listViewModel = ViewModelProvider(this, listViewModelFactory).get(ItemListViewModel::class.java)

        query = advertisementRepository.getOnSaleAdvertisements()
            .orderBy(Advertisement.FIELD_CREATION_DATE, Query.Direction.DESCENDING)

        if(accountViewModel.authenticationState.value == AccountViewModel.AuthenticationState.AUTHENTICATED
            && accountViewModel.user.value != null){
            listViewModel.currentUserId = accountViewModel.user.value!!.id!!
            options = FirestoreRecyclerOptions.Builder<Advertisement>().setQuery(query) { snapshot ->
                snapshot.toObject(Advertisement::class.java)!!.also {
                    if (it.userid.toString() != listViewModel.currentUserId) it.id = snapshot.id
                    else it.id = ""
                }
            }.build()
        }
        else{
            listViewModel.currentUserId = null
            options = FirestoreRecyclerOptions.Builder<Advertisement>().setQuery(query) { snapshot ->
                snapshot.toObject(Advertisement::class.java)!!.also {
                    it.id = snapshot.id
                }
            }.build()
        }

        // RecyclerView
        adapter = object : AdvertisementAdapter(this@OnSaleListFragment,
            this@OnSaleListFragment, this@OnSaleListFragment, advertisementRepository, options, listViewModel){
            override fun onDataChanged() {
                // Show/hide content if the query returns empty.
                if (itemCount == 0) {
                    binding.recyclerViewOnSale.visibility = View.GONE
                    binding.feedback.visibility = View.VISIBLE
                } else {
                    binding.recyclerViewOnSale.visibility = View.VISIBLE
                    binding.feedback.visibility = View.GONE
                }
            }

            override fun onError(e: FirebaseFirestoreException) {
                // Show a snackbar on errors
                Snackbar.make(binding.root,
                    "Error: check logs for info.", Snackbar.LENGTH_LONG).show()
                Log.d("kkk", "$this $e")
            }
        }
        binding.recyclerViewOnSale.adapter = adapter

        accountViewModel.authenticationState.observe(viewLifecycleOwner, Observer{ authenticationState ->
            if(authenticationState == AccountViewModel.AuthenticationState.AUTHENTICATED && accountViewModel.user.value != null){
                listViewModel.currentUserId = accountViewModel.user.value!!.id!!
                options = FirestoreRecyclerOptions.Builder<Advertisement>().setQuery(query) { snapshot ->
                    snapshot.toObject(Advertisement::class.java)!!.also {
                        if (it.userid.toString() != listViewModel.currentUserId) it.id = snapshot.id
                        else it.id = ""
                    }
                }.build()
                adapter.updateOptions(options)
                binding.recyclerViewOnSale.adapter = adapter
                adapter.listViewModel.currentUserId = accountViewModel.user.value!!.id!!
                binding.viewModel = listViewModel
                binding.passfilterViewModel.listViewModel = listViewModel
            }
            else{
                listViewModel.currentUserId = null
                adapter.listViewModel.currentUserId = null
                binding.viewModel = listViewModel
                binding.passfilterViewModel.listViewModel = listViewModel
                options = FirestoreRecyclerOptions.Builder<Advertisement>().setQuery(query) { snapshot ->
                    snapshot.toObject(Advertisement::class.java)!!.also {
                        it.id = snapshot.id
                    }
                }.build()
                adapter.updateOptions(options)
                binding.recyclerViewOnSale.adapter = adapter
            }
        })

        binding.viewModel = listViewModel
        binding.passfilterViewModel.listViewModel = listViewModel

        binding.recyclerViewOnSale.itemAnimator = SlideInUpAnimator()

        // Filter Dialog
        filterDialog = FilterDialogFragment(this)

        binding.filterBar.setOnClickListener { onFilterClicked() }
        binding.buttonClearFilter.setOnClickListener { onClearFilterClicked() }

        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
    }

    override fun onStart() {
        super.onStart()
        onFilter(listViewModel.filters)     // Apply filters
        adapter.startListening()            // Start listening for Firestore updates
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }

    private fun onFilterClicked() {
        // Show the dialog containing filter options
        filterDialog.show(requireActivity().supportFragmentManager, FilterDialogFragment.TAG)
    }

    private fun onClearFilterClicked() {
//        filterDialog.resetFilters()
        // Set header
        binding.textCurrentSearch.text = HtmlCompat.fromHtml(Filters.default.getSearchDescription(requireContext()),
            HtmlCompat.FROM_HTML_MODE_LEGACY)
        binding.textCurrentSortBy.text = Filters.default.getOrderDescription(requireContext())
        listViewModel.filters = Filters.default
        binding.passfilterViewModel.listViewModel = listViewModel
        onFilter(Filters.default)
    }

    override fun onFilter(filters: Filters) {
        // Construct query basic query
        var query: Query = advertisementRepository.getOnSaleAdvertisements()

        // Category (equality filter)
        if (filters.hasCategory()) {
            query = query.whereEqualTo(Advertisement.FIELD_CATEGORY, filters.category)
        }

        // Sort by (orderBy with direction)
        if (filters.hasSortBy()) {
            query = query.orderBy(filters.sortBy.toString(), filters.sortDirection)
        }

        options = FirestoreRecyclerOptions.Builder<Advertisement>().setQuery(query) { snapshot ->
            snapshot.toObject(Advertisement::class.java)!!.also {
                if (listViewModel.currentUserId != null){
                    if (it.userid.toString() != listViewModel.currentUserId){
                        // text search
                        if (filters.hasText()){
                            if (it.title.contains(filters.text!!, true) || it.description.contains(filters.text!!, true))
                                it.id = snapshot.id
                            else it.id = ""
                        } else{
                            it.id = snapshot.id
                        }
                    } else it.id = ""
                } else{
                    // text search
                    if (filters.hasText()){
                        if (it.title.contains(filters.text!!, true) || it.description.contains(filters.text!!, true))
                            it.id = snapshot.id
                        else it.id = ""
                    } else{
                        it.id = snapshot.id
                    }
                }
            }
        }.build()
        // Update the query
        adapter.updateOptions(options)
        binding.recyclerViewOnSale.adapter = adapter

        // Set header
        binding.textCurrentSearch.text = HtmlCompat.fromHtml(filters.getSearchDescription(requireContext()),
            HtmlCompat.FROM_HTML_MODE_LEGACY)
        binding.textCurrentSortBy.text = filters.getOrderDescription(requireContext())

        // Save filters
        listViewModel.filters = filters
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //CLOSE KEYBOARD if opened
        val view = requireActivity().currentFocus
        if (view != null) {
            val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(requireView().windowToken, 0)
        }
    }

    //listener click on an advertisement
    override fun advertisementClickHandler(item: Advertisement) {
        val b = Bundle()
        b.putSerializable("advertisement", item)
        findNavController().navigate(R.id.action_onSaleListFragment_to_itemDetailsFragment, b)
    }

    override fun editClickHandler(item: Advertisement) {
        return
    }

    override fun deleteClickHandler(item: Advertisement) {
        return
    }

}
