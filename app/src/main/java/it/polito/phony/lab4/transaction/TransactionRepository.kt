package it.polito.phony.lab4.transaction

import android.app.Activity
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.getField
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.user.repository.UserRepository
import java.util.concurrent.atomic.AtomicInteger

class TransactionRepository(private val activity: Activity, private val userRepo: UserRepository) {

    object TransactionID {
        private val c = AtomicInteger(0)
        val id: Int
            get() = c.incrementAndGet()
    }

    companion object {
        const val TAG = "TransactionRepository"
        const val COLLECTION = "transactions"
        const val FOLDER = "profile_imgs"
        const val EXTENSION = ".jpg"

        private var instance: TransactionRepository? = null
        fun getInstance(activity : Activity, userRepo: UserRepository) =
            instance ?: synchronized(this) {
                instance ?: TransactionRepository(activity, userRepo).also { instance = it }
            }
    }

    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val storage: StorageReference = FirebaseStorage.getInstance().reference

    fun getTransactionsBySellerID(_sellerID: String): Query {
       return db.collection(COLLECTION).whereEqualTo("sellerID", _sellerID)
    }

    fun getTransactionsByBuyerID(_buyerID: String): Query {
        return db.collection(COLLECTION).whereEqualTo("buyerID", _buyerID)
    }

    fun getTransactionByAdvId(advId: String) : Query{
        return db.collection(COLLECTION).whereEqualTo(Transaction.FIELD_ADV_ID, advId)
    }

    fun getTransactionRating(_transID: String): Float? {
        var transactionRating : Float? = null
        val transaction = db.collection(COLLECTION)
            .document(_transID)
            .get()
            .addOnSuccessListener { it: DocumentSnapshot? ->
                transactionRating = it?.getField("rating")

            }

        return transactionRating

    }

    fun addTransaction(newTrans: Transaction) {
        // Add Transaction to its collection
        db.collection(COLLECTION).document(newTrans.transID).set(newTrans)
    }

    fun addRating(newRating: Transaction){
        db.collection(COLLECTION).document(newRating.transID).update(
            Transaction.FIELD_HAS_REVIEW, newRating.hasReview,
            Transaction.FIELD_RATING, newRating.rating,
            Transaction.FIELD_COMMENT, newRating.comment
        )
    }


    fun removeTransaction(transToDelete: Transaction) {
        UserRepository.run {
            //isNegative = true
            //getInstance().updateRatings(transToDelete.sellerID, transToDelete.rating!!, true)
        }

        db.collection(COLLECTION).document(transToDelete.transID).delete()
    }

    //This function has to be called only by an user which has some articles on "sold"
    //and the corresponding Transaction

    /*fun getAvgRatingBySellerID(_sellerID: String): Float {
        val actualRate : Float? = UserRepository.getInstance().getUserTotalRating(_sellerID)
        val nRatings : Float? = UserRepository.getInstance().getUserTransTotNumber(_sellerID)

        return if(actualRate != null && nRatings != null && actualRate >=0 && nRatings > 0)
            actualRate.div(nRatings)
        else
            0F

    }*/

}