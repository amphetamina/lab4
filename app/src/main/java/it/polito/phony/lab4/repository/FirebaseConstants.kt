package it.polito.phony.lab4.repository

object FirebaseConstants {
    operator fun invoke(): FirebaseConstants {
        return this
    }

    val N_TRANSACTIONS_FIELD = "nTransactions"
    val RATING_FIELD = "rating"
    val SELLER_FIELD = "seller"
}