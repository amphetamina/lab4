package it.polito.phony.lab4.transaction

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentItemsOfInterestListBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.notification.Notification
import it.polito.phony.lab4.user.account.AccountViewModel

class ItemsOfInterestListFragment : Fragment(),
    InterestAdapter.OnAdvertisementClickListener,
    InterestAdapter.OnRemoveClickListener{

    private lateinit var advertisementRepository : AdvertisementRepository
    private lateinit var adapter: InterestAdapter
    private lateinit var query: Query
    lateinit var options: FirestoreRecyclerOptions<Advertisement>
    private val accountViewModel: AccountViewModel by activityViewModels()
    private lateinit var binding: FragmentItemsOfInterestListBinding
    private lateinit var notification : Notification

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notification = Notification(requireActivity())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("kkk","$this onCreateView")

        binding = FragmentItemsOfInterestListBinding.inflate(layoutInflater)
        binding.lifecycleOwner = viewLifecycleOwner

        val userId : String = if(accountViewModel.authenticationState.value == AccountViewModel.AuthenticationState.AUTHENTICATED
            && accountViewModel.user.value != null){
            accountViewModel.user.value!!.id!!
        } else{
            "-"
        }

        advertisementRepository = AdvertisementRepository.getInstance(requireActivity())

        query = advertisementRepository.getUserInterestAdvertisements(userId)

        options = FirestoreRecyclerOptions.Builder<Advertisement>().setQuery(query
        ) { snapshot ->
            snapshot.toObject(Advertisement::class.java)!!
                .also {
                    it.id = snapshot.id
                }
        }.build()
        adapter = object : InterestAdapter(options, requireActivity(),
            this@ItemsOfInterestListFragment,
        this@ItemsOfInterestListFragment) {
            override fun onDataChanged() {
                // Show/hide content if the query returns empty.
                if (itemCount == 0) {
                    binding.interestsRecyclerView.visibility = View.GONE
                    binding.emptyListItemsOfInterest.visibility = View.VISIBLE
                } else {
                    binding.interestsRecyclerView.visibility = View.VISIBLE
                    binding.emptyListItemsOfInterest.visibility = View.GONE
                }
            }

            override fun onError(e: FirebaseFirestoreException) {
                Snackbar.make(binding.root, "Error: check logs for info.", Snackbar.LENGTH_LONG).show()
                Log.d("ERROR-InterestList", e.toString())
            }
        }

        binding.interestsRecyclerView.adapter = adapter
        binding.interestsRecyclerView.layoutManager = LinearLayoutManager(context)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = findNavController()
        accountViewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when (authenticationState) {
                AccountViewModel.AuthenticationState.UNAUTHENTICATED -> navController.navigate(R.id.nav_sign_in)
                AccountViewModel.AuthenticationState.AUTHENTICATED ->
                    if(accountViewModel.user.value != null) {
                        query = advertisementRepository.getUserInterestAdvertisements(accountViewModel.user.value!!.id!!)
                        options = FirestoreRecyclerOptions.Builder<Advertisement>().setQuery(query
                        ) { snapshot ->
                            snapshot.toObject(Advertisement::class.java)!!
                                .also {
                                    it.id = snapshot.id
                                }
                        }.build()
                        adapter.updateOptions(options)
                        binding.interestsRecyclerView.adapter = adapter
                    }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()    // Start listening for Firestore updates
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }

    override fun removeClickHandler(item: Advertisement) {
        advertisementRepository.removeInterestedUser(item.id, accountViewModel.user.value!!.id!!)
        // refresh recycler view
        adapter.updateOptions(options)

        //NOTIFICATIONS UNSUBSCRIPTIONS
        val advId = item.id
        notification.unsubscribe("/topics/advSold$advId", this.toString())          //UNSUBSCRIBE FROM ADVERTISEMENT SOLD TOPIC
        notification.unsubscribe("/topics/advNoMoreOnSale$advId", this.toString())  //UNSUBSCRIBE FROM ADVERTISEMENT NO MORE ON SALE TOPIC (Blocked)
        notification.unsubscribe("/topics/advDeleted$advId", this.toString())       //UNSUBSCRIBE FROM ADVERTISEMENT DELETED TOPIC (Blocked)

        Toast.makeText(activity, "Request withdrawn", Toast.LENGTH_SHORT).show()
    }

    override fun advertisementClickHandler(item: Advertisement) {
        val b = Bundle()
        b.putSerializable("advertisement", item)
        findNavController().navigate(R.id.action_itemsOfInterestListFragment_to_itemDetailsFragment, b)
    }
}