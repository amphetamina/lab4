package it.polito.phony.lab4.item

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.core.graphics.drawable.toBitmap
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import it.polito.phony.lab4.itemlist.ItemListFragment
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentItemDetailsBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.itemlist.ItemListFragment.AdvertisementKey.ID_KEY
import it.polito.phony.lab4.itemlist.ItemListFragment.AdvertisementKey.IMAGE_ID_KEY
import it.polito.phony.lab4.itemlist.ItemListFragment.AdvertisementKey.MARKER_KEY
import it.polito.phony.lab4.notification.Notification
import it.polito.phony.lab4.request.ShowRequestsFragment.RequestKey.ADVERTISEMENT
import it.polito.phony.lab4.request.ShowRequestsFragment.RequestKey.INTERESTED_USERS
import it.polito.phony.lab4.user.account.AccountViewModel
import it.polito.phony.lab4.user.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class ItemDetailsFragment : Fragment(), OnMapReadyCallback {
    private lateinit var binding: FragmentItemDetailsBinding
    private lateinit var itemDetailsViewModel: ItemViewModel
    private lateinit var itemViewModelFactory: ItemViewModelFactory
    private lateinit var advItem : Advertisement
    private lateinit var img : Bitmap
    private lateinit var image : ImageView
    private var userId: String? = null                  //id of the user actually logged in
    private var isUserInterested: Boolean = false       //say if the user logged in is already interested to this advertisement
    private var ADV_PHOTO_PATH: String? = null
    private val accountViewModel: AccountViewModel by activityViewModels()
    private lateinit var advertisementRepository: AdvertisementRepository
    private lateinit var notification : Notification


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        advertisementRepository = AdvertisementRepository.getInstance(requireActivity())
        advItem = Advertisement()

        arguments?.let { bundle ->
            if ((bundle.get("advertisement") as Advertisement?) != null) {
                advItem = bundle.get("advertisement") as Advertisement
            }
            else if (bundle.get("advertisement_id") != null) {       //navigation from click on notification
                MainScope().launch(context = Dispatchers.IO) {
                    advItem = advertisementRepository.getAdvertisemntByIdAwait(bundle.get("advertisement_id").toString())!!
                }
            }
            else {
                advItem = Advertisement()
            }
        }

        img = resources.getDrawable(R.drawable.loading, requireActivity().theme).toBitmap()
        ADV_PHOTO_PATH = "${ItemListFragment.ADV_PHOTO_PREFIX}${advItem.id}.png"
        notification = Notification(requireActivity())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //CLOSE KEYBOARD if opened
        val view = requireActivity().currentFocus
        if (view != null) {
            val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(requireView().windowToken, 0)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        userId = accountViewModel.user.value?.id?: "-"

        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_item_details, container, false)
        itemViewModelFactory = ItemViewModelFactory(advItem, img, userId!!, advertisementRepository)
        itemDetailsViewModel = ViewModelProvider(this, itemViewModelFactory).get(ItemViewModel::class.java)
        binding.itemViewModel = itemDetailsViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        //If I am not the owner of the advertisement I have to distinguish 2 cases:
        //1) I have already sent a request for this advertisement (isUserInterested = true)
        //2) I haven't sent a request for the advertisement (isUserInterested = false)
        if(!userId.equals(advItem.userid.toString())){
            if(!itemDetailsViewModel.advertisement.value!!.interestedUsers.isNullOrEmpty()) {
                for (interestedUser in itemDetailsViewModel.advertisement.value!!.interestedUsers!!) {
                    if (interestedUser == userId) {
                        isUserInterested = true
                        break
                    }
                }
            }
        }

        image = binding.root.findViewById(R.id.itemImageView)
        itemDetailsViewModel.loadImage(advItem.id)

        //FAB
        setFabListener()
        updateFab()

        //CLICK ON IMAGE
        image.setOnClickListener{
            val b = bundleOf(
                IMAGE_ID_KEY to advItem.id)
            findNavController().navigate(R.id.action_itemDetailsFragment_to_itemImageFragment, b)
        }

        //MAP FRAGMENT
        val mapFragment : SupportMapFragment = childFragmentManager.findFragmentById(R.id.mainMap) as SupportMapFragment
        mapFragment.getMapAsync(this@ItemDetailsFragment)

        //ENABLE VERTICAL SCROLL ON MAP
        val mainScrollView : ScrollView =  binding.root.findViewById(R.id.main_scrollview)
        val transparentImageView : ImageView = binding.root.findViewById(R.id.transparent_image)
        transparentImageView.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(true)      // Disallow ScrollView to intercept touch events
                    return@setOnTouchListener false                                             //Disable touch on transparent view
                }
                MotionEvent.ACTION_UP -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(false)     // Allow ScrollView to intercept touch events
                    return@setOnTouchListener true
                }
                MotionEvent.ACTION_MOVE -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(true)
                    return@setOnTouchListener false
                }
                else -> return@setOnTouchListener true
            }
        }

        //LOAD SELLER PHOTO
        val userID = itemDetailsViewModel.advertisement.value?.userid.toString()
        try{
            val imageref = UserRepository.getInstance(requireActivity()).getUserImgReference(userID)
            Glide.with(binding.sellerImg.context)
                .load(imageref)
                .into(binding.sellerImg as ImageView)
        } catch(ex: Throwable) {
            Log.i("TAG", "$userID img not found")
        }

        //ON CLICK LISTENER SELLER
        binding.sellerCard.setOnClickListener{
            val b = bundleOf(
                "id" to binding.seller.text)
            findNavController().navigate(R.id.action_itemDetailsFragment_to_nav_show_profile, b)
        }

        return binding.root
    }

    override fun onMapReady(myMap: GoogleMap?) {
        try {
            val coordinates = itemDetailsViewModel.advertisement.value?.markerLat?.let {
                itemDetailsViewModel.advertisement.value?.markerLong?.let { it1 ->
                    LatLng(
                        it,
                        it1
                    )
                }
            }
            myMap?.addMarker(coordinates?.let {
                MarkerOptions()
                    .position(it).title("Item on sale")
            })
            myMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 16.0f))

            myMap?.setOnMapClickListener {
                val b = bundleOf(
                    MARKER_KEY to itemDetailsViewModel.advertisement.value?.markerLat?.let { it1 ->
                        itemDetailsViewModel.advertisement.value?.markerLong?.let { it2 ->
                            LatLng(
                                it1,
                                it2
                            )
                        }
                    }
                )
                findNavController().navigate(R.id.action_itemDetailsFragment_to_mapsFragment, b)
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)  {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.clear()
        inflater.inflate(R.menu.advertisement_menu, menu)
        val item = menu.findItem(R.id.action_edit)

        if(!userId.equals(advItem.userid.toString()) || (advItem.status == Advertisement.Status.Sold.toString())){
            item.isVisible = false
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            R.id.action_edit -> {
                val b = bundleOf(
                    ID_KEY to itemDetailsViewModel.advertisement.value!!.id,
                    "Action" to "Edit"
                )
                b.putSerializable("advertisement", itemDetailsViewModel.advertisement.value!!)
                findNavController().navigate(R.id.action_itemDetailsFragment_to_itemEditFragment2, b)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = findNavController()
        accountViewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when (authenticationState) {
                AccountViewModel.AuthenticationState.UNAUTHENTICATED -> navController.navigate(R.id.action_itemDetailsFragment_to_nav_sign_in)
                AccountViewModel.AuthenticationState.AUTHENTICATED ->
                    if(accountViewModel.user.value != null) {
                        userId = accountViewModel.user.value!!.id!!
                        updateFab()
                    }
            }
        })
    }

    private fun setFabListener(){
        //FAB
        val sendRequest: FloatingActionButton = binding.root.findViewById(R.id.send_request)
        val cancelRequest: FloatingActionButton = binding.root.findViewById(R.id.send_request_cancel)
        val showList: FloatingActionButton = binding.root.findViewById(R.id.show_requests)

        sendRequest.setOnClickListener{
            advertisementRepository.addInterestedUser(advItem.id, userId!!)
            isUserInterested = true

            updateFab()

            //NOTIFICATIONS SUBSCRIPTIONS
            val advId = advItem.id
            notification.subscribe("/topics/advSold$advId", this.toString())         //SUBSCRIBE TO ADVERTISEMENT SOLD TOPIC
            notification.subscribe("/topics/advNoMoreOnSale$advId", this.toString()) //SUBSCRIBE TO ADVERTISEMENT NO MORE ON SALE TOPIC (Blocked)
            notification.subscribe("/topics/advDeleted$advId", this.toString())      //SUBSCRIBE TO ADVERTISEMENT DELETED TOPIC (Blocked)
            notification.subscribe("/topics/advModified$advId", this.toString())      //SUBSCRIBE TO ADVERTISEMENT MODIFIED TOPIC

            //SEND USER INTERESTED NOTIFICATION
            val advTitle = advItem.title
            notification.send("/topics/newInterestedUsers$advId",
                "New User interested",
                "$userId made a request for the item $advTitle",
                "advertisement_id",
                advId,
                this.toString(),
                R.id.itemDetailsFragment)
            Toast.makeText(activity, "Request sent", Toast.LENGTH_SHORT).show()
        }

        cancelRequest.setOnClickListener{
            advertisementRepository.removeInterestedUser(advItem.id, userId!!)
            isUserInterested = false
            updateFab()

            //NOTIFICATIONS UNSUBSCRIPTIONS
            val advId = advItem.id
            notification.unsubscribe("/topics/advSold$advId", this.toString())           //UNSUBSCRIBE FROM ADVERTISEMENT SOLD TOPIC
            notification.unsubscribe("/topics/advNoMoreOnSale$advId", this.toString())   //UNSUBSCRIBE FROM ADVERTISEMENT NO MORE ON SALE TOPIC (Blocked)
            notification.unsubscribe("/topics/advDeleted$advId", this.toString())        //UNSUBSCRIBE FROM ADVERTISEMENT DELETED TOPIC (Blocked)
            notification.unsubscribe("/topics/advModified$advId", this.toString())      //SUBSCRIBE TO ADVERTISEMENT MODIFIED TOPIC
            // Toast.makeText(activity, "Request withdrawn", Toast.LENGTH_SHORT).show()
        }

        showList.setOnClickListener{
            val interestedUsers: List<String>?
            if (itemDetailsViewModel.advertisement.value!!.interestedUsers.isNullOrEmpty()) {
                interestedUsers = emptyList()
            } else {
                interestedUsers = itemDetailsViewModel.advertisement.value!!.interestedUsers
            }
            val b = bundleOf(
                INTERESTED_USERS to interestedUsers
            )
            b.putSerializable(ADVERTISEMENT, advItem)
            findNavController().navigate(
                R.id.action_itemDetailsFragment_to_showRequestsFragment,
                b
            )
        }
    }

    private fun updateFab(){
        val sendRequest: FloatingActionButton = binding.root.findViewById(R.id.send_request)
        val cancelRequest: FloatingActionButton = binding.root.findViewById(R.id.send_request_cancel)
        val showList: FloatingActionButton = binding.root.findViewById(R.id.show_requests)

        if(userId.equals("-") || (advItem.status == Advertisement.Status.Sold.toString())) {
            sendRequest.hide()
            cancelRequest.hide()
            showList.hide()
            return
        }

        //If the user logged in is the owner of the advertisement
        if (userId.equals(advItem.userid.toString())){
            sendRequest.hide()
            cancelRequest.hide()
            showList.show()
            return
        }

        //if user logged in isn't already interested, he send a request
        if (!isUserInterested){
            sendRequest.show()
            cancelRequest.hide()
            showList.hide()
            return
        }

        //if user logged in is already interested, he withdraw his interest
        sendRequest.hide()
        cancelRequest.show()
        showList.hide()
        return
    }
}

