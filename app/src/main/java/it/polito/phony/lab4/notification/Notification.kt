package it.polito.phony.lab4.notification

import android.app.Activity
import android.util.Log
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.messaging.FirebaseMessaging
import org.json.JSONException
import org.json.JSONObject

class Notification (val activity : Activity) {

    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverKey = "key=" + "AAAAskQUtt8:APA91bG-ceWgYDNl9kGIlbNTbA3o3Iu0vOi_63EX3QnnxrBRCQMouJQ15W05OAhfl5_jJAXZrrJnl23h_YMI2iwW9wXA53TipHBg2JeIHiJe5m5Z9JCr6b42Jf6t55ws9V5pmoT1PvGG"
    private val contentType = "application/json"
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(activity.applicationContext)
    }

    fun subscribe(topic : String, caller: String){
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
        Log.d("TAG", "$caller subscribed to topic $topic")
    }

    fun send(topic: String, title: String, message: String, paramName: String, paramValue: String, caller: String, clickDestination: Int){
        val notification = JSONObject()
        val notificationBody = JSONObject()
        try {
            notificationBody.put("title", title)
            notificationBody.put("message", message)
            notificationBody.put("paramName", paramName)
            notificationBody.put("paramValue", paramValue)
            notificationBody.put("destination", clickDestination)
            notification.put("to", topic)
            notification.put("data", notificationBody)
        } catch (e: JSONException) {
            Log.e("TAG", "exception : " + e.message)
        }
        val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
            Response.Listener<JSONObject> { response ->
                Log.i("TAG", "$caller onResponse: $response")
            },
            Response.ErrorListener {
                Toast.makeText(activity.applicationContext, "Request error", Toast.LENGTH_LONG).show()
                Log.i("TAG", "$caller onErrorResponse: Didn't work")
            }) {

            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverKey
                params["Content-Type"] = contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }


    fun unsubscribe(topic: String, caller: String){
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
        Log.d("TAG", "$caller unsubscribed from topic $topic")
    }

}