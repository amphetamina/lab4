package it.polito.phony.lab4.transaction

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import it.polito.phony.lab4.databinding.ItemOfInterestBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository

open class InterestAdapter(options: FirestoreRecyclerOptions<Advertisement>,
                           val activity: Activity,
                           private val clickListener: OnAdvertisementClickListener,
                           private val removeClickListener: OnRemoveClickListener) :
    FirestoreRecyclerAdapter<Advertisement, InterestAdapter.ViewHolder>(options) {

    interface OnAdvertisementClickListener{
        fun advertisementClickHandler(item: Advertisement)
    }

    interface OnRemoveClickListener{
        fun removeClickHandler(item: Advertisement)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            ItemOfInterestBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    class ViewHolder(val binding: ItemOfInterestBinding): RecyclerView.ViewHolder(binding.root) {

    }

    override fun onBindViewHolder(
        holder:  ViewHolder,
        position: Int,
        model: Advertisement
    ) {
        holder.apply{
            binding.item = model
            binding.interestItemPrice.text = model.getPriceStr()
            binding.interestUsersCount.text = if (model.interestedUsers != null) model.interestedUsers.size.toString() else "0"

            //LOAD IMAGE
            val id = model.id
            try {
                val imgRef = AdvertisementRepository.getInstance(activity).getAdvertisementImg(id)
                Glide.with(binding.interestItemImage.context).load(imgRef).into(binding.interestItemImage)
            } catch (ex: Throwable) {
                Log.i("InterestAdapter", "$id img not found")
            }

            // REMOVE INTEREST
            binding.removeInterest.setOnClickListener {
                removeClickListener.removeClickHandler(model)
            }
            //ADVERTISEMENT CARD LISTENER
            binding.interestItemCard.setOnClickListener {
                clickListener.advertisementClickHandler(model)
            }
        }
    }
}