package it.polito.phony.lab4

import android.graphics.Bitmap
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.navigation.findNavController
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.drawable.toBitmap
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.ui.*
import com.firebase.ui.auth.AuthUI
import com.google.android.material.snackbar.Snackbar
import it.polito.phony.lab4.databinding.ActivityMainBinding
import it.polito.phony.lab4.databinding.NavHeaderMainBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.item.ItemViewModel
import it.polito.phony.lab4.item.ItemViewModelFactory
import it.polito.phony.lab4.user.account.AccountViewModel
import it.polito.phony.lab4.user.account.AccountViewModelFactory
import it.polito.phony.lab4.itemlist.ItemListViewModel
import it.polito.phony.lab4.itemlist.ItemListViewModelFactory
import it.polito.phony.lab4.notification.Notification
import it.polito.phony.lab4.transaction.Transaction
import it.polito.phony.lab4.transaction.TransactionRepository
import it.polito.phony.lab4.user.repository.UserRepository


class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var navHeaderBinding: NavHeaderMainBinding
    private lateinit var itemViewModel: ItemViewModel
    private lateinit var itemViewModelFactory: ItemViewModelFactory
    private lateinit var notification : Notification
    private val listViewModel: ItemListViewModel by viewModels { ItemListViewModelFactory(false)}
    private lateinit var accountViewModel: AccountViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navHeaderBinding = DataBindingUtil.inflate(layoutInflater, R.layout.nav_header_main, activityMainBinding.navView, false)
        activityMainBinding.navView.addHeaderView(navHeaderBinding.root)
        val navController = findNavController(R.id.nav_host_fragment)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        NavigationUI.setupActionBarWithNavController(this, navController, activityMainBinding.drawerLayout)
        NavigationUI.setupWithNavController(activityMainBinding.navView, navController)
        val userRepository: UserRepository = UserRepository.getInstance(this)
        accountViewModel = ViewModelProvider(this, AccountViewModelFactory(userRepository)).get(AccountViewModel::class.java)
        initItemViewModel()
        activityMainBinding.itemViewModel = itemViewModel
        activityMainBinding.lifecycleOwner = this
        navHeaderBinding.accountViewModel = accountViewModel
        navHeaderBinding.lifecycleOwner = this
        observeAuthState()
        notification = Notification(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return NavigationUI.navigateUp(navController, activityMainBinding.drawerLayout) || super.onSupportNavigateUp()
    }

    private fun initItemViewModel(){
        val img : Bitmap = resources.getDrawable(R.drawable.doge, theme).toBitmap()
        itemViewModelFactory = ItemViewModelFactory(Advertisement(), img, "BOH", AdvertisementRepository.getInstance(this))
        itemViewModel = ViewModelProvider(this, itemViewModelFactory).get(ItemViewModel::class.java)
    }

    private fun observeAuthState() {

        accountViewModel.toastmsg.observe(this, Observer {
            it?.let {
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                accountViewModel.onShowToastMsgComplete()
            }
        })
        accountViewModel.snackmsg.observe(this, Observer {
            it?.let {
                Snackbar.make(activityMainBinding.root, it, Snackbar.LENGTH_SHORT).show()
                accountViewModel.onShowSnackMsgComplete()
            }
        })

        accountViewModel.user.observe(this, Observer { user ->
            if (user != null) {
                activityMainBinding.navView.menu.findItem(R.id.nav_show_profile).isVisible = true
                activityMainBinding.navView.menu.findItem(R.id.nav_sign_in).isVisible = false
                activityMainBinding.navView.menu.findItem(R.id.itemListFragment).isVisible = true
                activityMainBinding.navView.menu.findItem(R.id.itemsOfInterestListFragment).isVisible = true
                activityMainBinding.navView.menu.findItem(R.id.boughtItemsListFragment).isVisible = true
                activityMainBinding.navView.menu.findItem(R.id.soldItemsListFragment).isVisible = true

                if(accountViewModel.user.value?.advertisements != null) {
                    for(adv in accountViewModel.user.value?.advertisements!!){
                        //SUBSCRIBE TO INTERESTED USERS TOPIC
                        notification.subscribe("/topics/newInterestedUsers$adv", this.toString())
                    }
                }

                if(accountViewModel.user.value?.advertisementsInterestedTo != null) {
                    for (adv in accountViewModel.user.value?.advertisementsInterestedTo!!) {
                        notification.subscribe("/topics/advSold$adv", this.toString())         //SUBSCRIBE TO ADVERTISEMENT SOLD TOPIC
                        notification.subscribe("/topics/advNoMoreOnSale$adv", this.toString()) //SUBSCRIBE TO ADVERTISEMENT NO MORE ON SALE TOPIC (Blocked)
                        notification.subscribe("/topics/advDeleted$adv", this.toString())      //SUBSCRIBE TO ADVERTISEMENT DELETED TOPIC (Blocked)
                        notification.subscribe("/topics/advModified$adv", this.toString())      //SUBSCRIBE TO ADVERTISEMENT MODIFIED TOPIC
                    }
                }

                //SUBSCRIBE TO ADVERTISEMENT REVIEWED TOPIC
                accountViewModel.user.value?.id?.let {
                    TransactionRepository.getInstance(this, UserRepository.getInstance(this))
                        .getTransactionsBySellerID(it)
                        .get()
                        .addOnSuccessListener { querySnapshot ->
                            for (transaction in querySnapshot) {
                                val advId = transaction.toObject(Transaction::class.java).advID
                                notification.subscribe("/topics/advReviewed$advId", this.toString())
                            }
                        }
                }

            } else {
                activityMainBinding.navView.menu.findItem(R.id.nav_show_profile).isVisible = false
                activityMainBinding.navView.menu.findItem(R.id.nav_sign_in).isVisible = true
                activityMainBinding.navView.menu.findItem(R.id.itemListFragment).isVisible = false
                activityMainBinding.navView.menu.findItem(R.id.itemsOfInterestListFragment).isVisible = false
                activityMainBinding.navView.menu.findItem(R.id.boughtItemsListFragment).isVisible = false
                activityMainBinding.navView.menu.findItem(R.id.soldItemsListFragment).isVisible = false
            }
        })

        accountViewModel.logOut.observe(this, Observer { logOut ->
            if (logOut == true) {
                AuthUI.getInstance().signOut(this)
                accountViewModel.logOutComplete()
            }
        })
    }
}
