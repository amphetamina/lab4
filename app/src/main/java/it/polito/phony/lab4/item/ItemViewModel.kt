package it.polito.phony.lab4.item

import android.graphics.*
import android.os.Build
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.*
import java.util.*

class ItemViewModel(item: Advertisement, img: Bitmap, currentUserId: String, private val advertisementRepository: AdvertisementRepository): ViewModel() {

    val editedTitle = MutableLiveData<String>()
    val editedDescription = MutableLiveData<String>()
    val editedPrice = MutableLiveData<String>()
    val editedCategoryName = MutableLiveData<String>()
    val editedSubCategoryName = MutableLiveData<String>()
    val editedLocation= MutableLiveData<String>()
    val editedExpirationDate = MutableLiveData<String>()
    val editedImg = MutableLiveData<Bitmap>()
    val photoPath = MutableLiveData<String>()
    val isOwner = MutableLiveData<Boolean?>()
    val editedStatus = MutableLiveData<String>()
    val editedMarkerLat = MutableLiveData<Double>()
    val editedMarkerLong = MutableLiveData<Double>()


    private val _priceWithCurrency by lazy {
        return@lazy MutableLiveData<String>(if (advertisement.value != null) advertisement.value!!.price.toInt().toString()+" €" else " ")
    }
    val priceWithCurrency: LiveData<String>
        get() = _priceWithCurrency

    private val _itemExpDate by lazy {
        return@lazy MutableLiveData<String>(if (advertisement.value != null) getExpDateStr(advertisement.value!!.expirationdate) else " ")
    }
    val itemExpDate: LiveData<String>
        get() = _itemExpDate

    private val _category by lazy {
        return@lazy MutableLiveData<String>(if(item.category.isNotEmpty()) item.category else "")
    }
    val category: LiveData<String>
        get() = _category

    private val _location by lazy {
        return@lazy MutableLiveData<String>(if(item.location.isNotEmpty()) item.location else "")
    }
    val location: LiveData<String>
        get() = _location

    private val _img by lazy {
        return@lazy MutableLiveData<Bitmap>(img)
    }
    val img: LiveData<Bitmap>
        get() = _img

    private val _isImgEdited by lazy {
        return@lazy MutableLiveData<Boolean>(false)
    }
    val isImgEdited: LiveData<Boolean>
        get() = _isImgEdited

    private val _status by lazy {
        return@lazy MutableLiveData<String>(if(item.category.isNotEmpty()) item.category else "")
    }
    val status: LiveData<String>
        get() = _status

    private val _markerLat by lazy {
        return@lazy MutableLiveData<Double>(item.markerLat)
    }
    val markerLat: LiveData<Double>
        get() = _markerLat

    private val _markerLong by lazy {
        return@lazy MutableLiveData<Double>(item.markerLong)
    }
    val markerLong: LiveData<Double>
        get() = _markerLong

    private val _advertisement by lazy {
        return@lazy MutableLiveData<Advertisement>(item).also {
            if (!item.id.isEmpty() && item.title.isEmpty()) {
                viewModelScope.launch { loadAdvertisement(item.id, currentUserId) }
            }else{
                isOwner.value = item.userid.toString() == currentUserId
            }
        }
    }
    val advertisement: LiveData<Advertisement>
        get() = _advertisement


    //GET DATA + SNAPSHOT LISTENER
    private fun loadAdvertisement(id:String, currentUserId: String) {
        val advRef = advertisementRepository.getAdvertisementById(id)

        advRef.addSnapshotListener { snapshot, e->
            if (e != null) {
                Log.w("kkk", "Listen failed.", e)
                return@addSnapshotListener
            }
            if (snapshot != null && snapshot.exists()) {
                Log.d("kkk", "Current data: ${snapshot.data}")
                _advertisement.value = snapshot.toObject(Advertisement::class.java)
                _advertisement.value!!.withDocumentKey(snapshot.id)
                _priceWithCurrency.value = advertisement.value!!.price.toString()
                _itemExpDate.value = getExpDateStr(advertisement.value!!.expirationdate)
                isOwner.value =  _advertisement.value!!.userid.toString() == currentUserId
                _status.value = _advertisement.value!!.status
                _markerLat.value = _advertisement.value!!.markerLat
                _markerLong.value = _advertisement.value!!.markerLong
                _location.value = _advertisement.value!!.location
                onEditItem()
            } else {
                Log.d("kkk", "Current data: null")
            }
        }
    }

    init {
        onEditItem()
    }

    fun loadImage(idImmagine: String?) {
        if (idImmagine != null) {
            viewModelScope.launch(context = Dispatchers.IO) {
                try {
                    val imgTask = advertisementRepository.getAdvertisementImg(idImmagine).stream
                    Tasks.await(imgTask).let {
                        _img.postValue(BitmapFactory.decodeStream(it.stream))
                    }
                } catch (ex: Throwable) {
                    Log.i("ItemViewModel", "$idImmagine img loading fail")
                }

            }
        }
    }

    fun loadEditImg(imgId: String?) {
        if (imgId != null) {
            viewModelScope.launch(context = Dispatchers.IO) {
                try {
                    val imgTask =
                        advertisementRepository.getAdvertisementImg(imgId).stream
                    Tasks.await(imgTask).let {
                        editedImg.postValue(BitmapFactory.decodeStream(it.stream))
                    }
                } catch (error: Throwable) {
                    Log.i("ItemViewModel", "$this $imgId not found")
                }
            }
        }
    }

    /*fun loadSellerImg(imgId: String?) {
        if (imgId != null) {
            viewModelScope.launch(context = Dispatchers.IO) {
                try {
                    val imgTask =
                        profileRepository.getAdvertisementImg(imgId).stream
                    Tasks.await(imgTask).let {
                        editedImg.postValue(BitmapFactory.decodeStream(it.stream))
                    }
                } catch (error: Throwable) {
                    Log.i("ItemViewModel", "$this $imgId not found")
                }
            }
        }
    }*/

    fun onEditItem() {
        editedTitle.value = advertisement.value!!.title
        editedDescription.value = advertisement.value!!.description
        editedLocation.value = advertisement.value!!.location
        editedPrice.value = if (advertisement.value!!.price > 0.0) advertisement.value!!.price.toString() else ""
        editedCategoryName.value = advertisement.value!!.category
        editedSubCategoryName.value = advertisement.value!!.subcategory
        editedExpirationDate.value = getExpDateStr(advertisement.value!!.expirationdate)
        editedImg.value = _img.value
        editedStatus.value = advertisement.value!!.status
        editedMarkerLat.value = advertisement.value!!.markerLat
        editedMarkerLong.value = advertisement.value!!.markerLong
    }

    private fun getExpDateStr(date: Date?): String{
        if(date != null){
            val year = (date.year + 1900).toString()
            val month = if (date.month + 1 > 9) (date.month + 1).toString() else "0" + (date.month + 1).toString()
            val day = if (date.date <= 9) "0"+(date.date).toString() else (date.date).toString()
            return "$year-$month-$day"
        }
        return " "
    }

    fun checkTitle(itemTitle: String) : String?{
        if (itemTitle.isEmpty())
            return "Please insert a valid title"
        return null
    }

    fun checkDescription(itemDescription: String) : String?{
        if (itemDescription.isEmpty())
            return "Please insert a valid description"
        return null
    }

    fun checkLocation(itemLocation: String) : String?{
        if (itemLocation.isEmpty())
            return "Please insert a valid location"
        return null
    }

    fun checkPrice(itemPrice: String) : String?{
        if (itemPrice.isEmpty())
            return "Please insert a valid price"
        val setPrice = itemPrice.toFloatOrNull()
        if (setPrice == null || setPrice <= 0f) return "Please insert a valid price"
        if (itemPrice.contains(".")){
            val priceStr = itemPrice.split(".")
            if (priceStr[1].isEmpty() || priceStr[1].length > 2) return "Please insert a valid price"
        }
        return null
    }

    fun checkQuantity(itemQuantity: String) : String?{
        if (itemQuantity.isEmpty())
            return "Please insert a valid quantity"
        val setQuantity = itemQuantity.toIntOrNull()
        if (setQuantity == null || setQuantity <= 0) return "Please insert a valid quantity"
        return null
    }

    fun checkExpDate(itemDate: String) : String?{
        if (itemDate.isEmpty() || itemDate == " ")
            return "Please insert a valid expiration date"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val date = LocalDate.parse(itemDate)
            val today = LocalDate.now()
            if (date.isBefore(today)  || date.isEqual(today))
                return "Please insert a valid expiration date"
        } else {
            val date = SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(itemDate)
            val today = Date()
            if (date!!.before(today) || date == today)
                return "Please insert a valid expiration date"
        }
        return null
    }

    fun checkImage(itemTitle: String) : String?{
        if (itemTitle.isEmpty())
            return "Please insert a valid title"
        return null
    }

    fun setImg(img: Bitmap, targetW: Int, targetH: Int) {
        var d: Float = img.height.coerceAtMost(img.width).toFloat()
        val scale: Float = (d / 800)
        if (scale != 0F) {
            d /= scale
            val width = img.width / scale
            val height = img.height / scale
            val newImg = Bitmap.createScaledBitmap(img, width.toInt(), height.toInt(), false)
            editedImg.value = newImg
        }
        else {
            editedImg.value = img
        }
    }

    fun setCurrentPhotoPath(path: String) {
        photoPath.value = path
    }

    fun rotateImgClockwise() {
        val matrix = Matrix()
        matrix.postRotate(+90.00F)
        /*editedImg.value = Bitmap.createBitmap(
            editedImg.value!!, 0, 0, editedImg.value!!.width, editedImg.value!!.height,
            matrix, true
        )*/
        val newBitmap = editedImg.value!!.copy(editedImg.value!!.config, true)
        val rotatedBitmap = Bitmap.createBitmap(
            editedImg.value!!, 0, 0, editedImg.value!!.width, editedImg.value!!.height,
            matrix, true
        )
        val canvas = Canvas(newBitmap)
        canvas.drawColor(Color.WHITE)
        //val src = Rect(0, 0, newBitmap.width, newBitmap.height)
        val dest = Rect(0, 0, newBitmap.width, newBitmap.height)
        canvas.drawBitmap(rotatedBitmap, null, dest, null)
        canvas.save()
        editedImg.value = rotatedBitmap
        _isImgEdited.value = true
    }

    fun rotateImgAntiClockwise() {
        val matrix = Matrix()
        matrix.postRotate(-90.00F)
        /*editedImg.value = Bitmap.createBitmap(
            editedImg.value!!, 0, 0, editedImg.value!!.width, editedImg.value!!.height,
            matrix, true
        )*/
        val newBitmap = editedImg.value!!.copy(editedImg.value!!.config, true)
        val rotatedBitmap = Bitmap.createBitmap(
            editedImg.value!!, 0, 0, editedImg.value!!.width, editedImg.value!!.height,
            matrix, true
        )
        val canvas = Canvas(newBitmap)
        canvas.drawColor(Color.WHITE)
        //val src = Rect(0, 0, newBitmap.width, newBitmap.height)
        val dest = Rect(0, 0, newBitmap.width, newBitmap.height)
        canvas.drawBitmap(rotatedBitmap, null, dest, null)
        canvas.save()
        editedImg.value = rotatedBitmap
        _isImgEdited.value = true
    }

    private val _isSaved = MutableLiveData(false)
    val isSaved: LiveData<Boolean>
        get() = _isSaved

    private val _isSaving = MutableLiveData(false)
    val isSaving: LiveData<Boolean>
        get() = _isSaving

    private val _toastMsg = MutableLiveData<String?>()
    val toastMsg: LiveData<String?>
        get() = _toastMsg

    fun saveAdvertisement(id: String, userId: String, advItem: Advertisement, newAdv: Advertisement) {
        viewModelScope.launch(context = Dispatchers.IO) {
            try {
                _isSaving.postValue(true)
                _isSaved.postValue(false)
                val task: Task<Void>
                //WRITE IN DB
                if (advItem.id.isEmpty()){
                    newAdv.id = id
                    task = advertisementRepository.addAdvertisement(userId, id, newAdv)
                }
                else{
                    task = advertisementRepository.updateAdvertisement(userId, advItem.id, newAdv)
                }

                Tasks.await(task)

                val imgTask = advertisementRepository.setAdvertisementImg(id, editedImg.value!!)
                Tasks.await(imgTask)

                _isSaved.postValue(true)

            } catch (e: Throwable) {
                e.printStackTrace()
                _toastMsg.postValue("Advertisement save fail")
                Log.e("ItemEditFragment", "${e.message}")
            } finally {
                _isSaving.postValue(false)
            }
        }

    }
}