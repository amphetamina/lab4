package it.polito.phony.lab4.itemlist

import androidx.lifecycle.ViewModel

class ItemListViewModel(isOwner: Boolean) : ViewModel() {
    var isOwner: Boolean = isOwner
    var currentUserId : String? = null
    var filters: Filters = Filters.default
}