package it.polito.phony.lab4.transaction

import java.io.Serializable

data class Transaction(
    var transID: String = "",
    val sellerID: String = "",
    val buyerID: String = "",
    val advID: String =  "",
    val advTitle: String = "",
    val totalPrice: String = "",
    val hasReview: Boolean = false,
    val rating: Float? = null,
    val comment: String? = null
): Serializable {

    // constructor for setting the buyer
    constructor(transID: String, sellerID: String, buyerID: String, advID: String, advTitle: String, totalPrice: String)
            : this(transID, sellerID, buyerID, advID, advTitle, totalPrice, false, null, null)
    {
    }

    // constructor for setting the rating
    constructor(transID: String,  hasReview: Boolean, rating: Float, comment: String?) :
            this(transID, "", "", "", "", "", hasReview, rating, comment){

    }

    companion object {
        const val FIELD_ADV_ID = "advID"
        const val FIELD_RATING = "rating"
        const val FIELD_HAS_REVIEW = "hasReview"
        const val FIELD_COMMENT = "comment"
    }
}