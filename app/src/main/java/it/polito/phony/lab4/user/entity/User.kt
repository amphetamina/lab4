package it.polito.phony.lab4.user.entity

import java.io.Serializable
import kotlin.collections.ArrayList

data class User (
    var id: String? = null,
    var name: String? = null,
    var birthdate: String? = null,
    var nickname: String? = null,
    var email: String? = null,
    var location : String? = null,
    var phonenumber: String? = null,
    var advertisements: List<Any?>? = ArrayList(),
    var advertisementsInterestedTo: List<String?>? = null,
    var totalRatings : Float? = 0F,
    var nTransactions: Int? = 0,
    var isSeller: Boolean? = false,
    var markerLat : Double? = null,
    var markerLong : Double? = null
    ) : Serializable