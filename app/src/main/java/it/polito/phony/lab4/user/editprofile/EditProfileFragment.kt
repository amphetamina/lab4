package it.polito.phony.lab4.user.editprofile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.material.snackbar.Snackbar
import it.polito.phony.lab4.BuildConfig
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentEditProfileBinding
import it.polito.phony.lab4.itemlist.ItemListFragment
import it.polito.phony.lab4.user.account.AccountViewModel
import it.polito.phony.lab4.user.entity.User
import it.polito.phony.lab4.user.profile.ProfileKey
import it.polito.phony.lab4.user.repository.UserRepository
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class EditProfileFragment : Fragment(), OnMapReadyCallback {

    // todo change keyboard behaviour

    companion object {
        const val TAG = "EditProfileFragment"
    }

    private val accountViewModel by activityViewModels<AccountViewModel>()
    private lateinit var editProfileViewModel: EditProfileViewModel
    private lateinit var tempFolder: File
    private lateinit var binding: FragmentEditProfileBinding
    private var googleMap : GoogleMap? = null
    private var marker : Marker? = null
    private var autocompleteFragment : AutocompleteSupportFragment? = null
    private var restoredLat : Double? = null
    private var restoredLong : Double? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_profile, container, false)
        val user: User? = arguments?.getSerializable("user") as User?
        val img: Bitmap? = arguments?.getParcelable("img")
        val userRepository: UserRepository = UserRepository.getInstance(requireActivity())
        editProfileViewModel = ViewModelProvider(this, EditProfileViewModelFactory(user, img, userRepository)).get(EditProfileViewModel::class.java)
        binding.editProfileViewModel = editProfileViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        registerForContextMenu(binding.cameraButton)
        registerForContextMenu(binding.userImage)
        binding.userImage.setOnClickListener{
                view -> view.showContextMenu()
        }
        binding.cameraButton.setOnClickListener{
                view -> view.showContextMenu()
        }
        binding.userImage.setOnLongClickListener{
                view -> view.showContextMenu()
        }
        binding.cameraButton.setOnLongClickListener{
                view -> view.showContextMenu()
        }

        binding.userBirthDate.setOnClickListener {

            val cldr = Calendar.getInstance()
            var day = cldr[Calendar.DAY_OF_MONTH]
            var month = cldr[Calendar.MONTH]
            var year = cldr[Calendar.YEAR]

            if (!editProfileViewModel.birthdate.value.isNullOrBlank()) {
                val editedBirthDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(editProfileViewModel.birthdate.value!!)
                cldr.time = editedBirthDate!!
                day = cldr.get(Calendar.DAY_OF_MONTH)
                month = cldr.get(Calendar.MONTH)
                year = cldr.get(Calendar.YEAR)
            }

            //DATE PICKER DIALOG
            val picker = DatePickerDialog(
                requireContext(),
                DatePickerDialog.OnDateSetListener { _, year2, monthOfYear, dayOfMonth ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        editProfileViewModel.setBirthDate(LocalDate.of(year2, monthOfYear+1, dayOfMonth).toString())
                    } else {
                        val MoY = if(monthOfYear < 10) "0${monthOfYear+1}" else "${monthOfYear+1}"
                        val DoM = if(dayOfMonth < 10) "0${dayOfMonth}" else "$dayOfMonth"
                        val dateString = "$year2-$MoY-$DoM"
                        editProfileViewModel.setBirthDate(dateString)
                    }
                },
                year,
                month,
                day
            )
            picker.show()
        }


        tempFolder = File(requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString()+"/temp")
        tempFolder.mkdir()

        //MAP FRAGMENT
        val mapFragment : SupportMapFragment = childFragmentManager.findFragmentById(R.id.mainMap) as SupportMapFragment
        mapFragment.getMapAsync(this@EditProfileFragment)

        //ENABLE VERTICAL SCROLL ON MAP
        val mainScrollView : ScrollView =  binding.root.findViewById(R.id.main_scrollview) as ScrollView
        val transparentImageView : ImageView = binding.root.findViewById(R.id.transparent_image) as ImageView
        transparentImageView.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(true)      // Disallow ScrollView to intercept touch events
                    return@setOnTouchListener false                                             //Disable touch on transparent view
                }
                MotionEvent.ACTION_UP -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(false)     // Allow ScrollView to intercept touch events
                    return@setOnTouchListener true
                }
                MotionEvent.ACTION_MOVE -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(true)
                    return@setOnTouchListener false
                }
                else -> return@setOnTouchListener true
            }
        }

        if (!Places.isInitialized()) {
            Places.initialize(requireActivity().applicationContext, getString(R.string.google_maps_key))
        }
        val placeFields : List<Place.Field> = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)
        autocompleteFragment = childFragmentManager.findFragmentById(R.id.autocomplete_fragment) as? AutocompleteSupportFragment
        autocompleteFragment?.setPlaceFields(placeFields)

        autocompleteFragment?.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                if(place.latLng != null) {
                    setMarkerFromLatLng(place.latLng!!.latitude, place.latLng!!.longitude)
                }
            }

            override fun onError(status: Status) {
                Log.i(TAG, "An error occurred: $status")
            }
        })

        return binding.root
    }

    override fun onMapReady(myMap: GoogleMap?) {
        googleMap = myMap
        try {
            val location = autocompleteFragment?.view?.findViewById(R.id.places_autocomplete_search_input) as EditText
            if(location.text.toString().isEmpty() || location.text.toString().isBlank()) {
                if (editProfileViewModel.user.value?.markerLat != null && editProfileViewModel.user.value?.markerLong != null) {
                    setMarkerFromLatLng(
                        editProfileViewModel.user.value?.markerLat!!,
                        editProfileViewModel.user.value?.markerLong!!
                    )
                    setLocationFromLatLng(
                        editProfileViewModel.user.value?.markerLat!!,
                        editProfileViewModel.user.value?.markerLong!!
                    )
                }
            }

            //TO HANDLE DEVICE ROTATION
            else{
                restoredLat?.let {
                    restoredLong?.let { it1 ->
                        setMarkerFromLatLng(
                            it,
                            it1
                        )
                    }
                }
            }

            //ON MAP CLICK LISTENER
            myMap?.setOnMapClickListener {
                setMarkerFromLatLng(
                    it.latitude,
                    it.longitude
                )
                setLocationFromLatLng(
                    it.latitude,
                    it.longitude
                )
            }

        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    //FRAGMENT XML TAG DOESN'T SUPPORT DATA BINDING SI I USE THE FOLLOWING 2 METHODS TO SAVE AND RESTORE THE STATE DURNG ROTATION
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val location = autocompleteFragment?.view?.findViewById(R.id.places_autocomplete_search_input) as EditText
        outState.putString(ItemListFragment.PROFILE_LOCATION_KEY, location.text.toString())
        if( marker?.position?.latitude != null) {
            outState.putDouble(ItemListFragment.PROFILE_LATITUDE_KEY, marker?.position?.latitude!!)
            outState.putDouble(ItemListFragment.PROFILE_LONGITUDE_KEY, marker?.position?.longitude!!)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            autocompleteFragment?.setText(savedInstanceState.getString(ItemListFragment.PROFILE_LOCATION_KEY))
            restoredLat = savedInstanceState.getDouble(ItemListFragment.PROFILE_LATITUDE_KEY)
            restoredLong = savedInstanceState.getDouble(ItemListFragment.PROFILE_LONGITUDE_KEY)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {
            autocompleteFragment?.setText(savedInstanceState.getString(ItemListFragment.PROFILE_LOCATION_KEY))
            restoredLat = savedInstanceState.getDouble(ItemListFragment.PROFILE_LATITUDE_KEY)
            restoredLong = savedInstanceState.getDouble(ItemListFragment.PROFILE_LONGITUDE_KEY)
        }

        editProfileViewModel.toastmsg.observe(viewLifecycleOwner, Observer { msg ->
            msg?.let {
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
                editProfileViewModel.onShowToastMsgComplete()
            }
        })

        editProfileViewModel.snackmsg.observe(viewLifecycleOwner, Observer { msg ->
            msg?.let {
                Snackbar.make(requireView(), msg, Snackbar.LENGTH_SHORT).show()
                editProfileViewModel.onShowSnackMsgComplete()
            }
        })

        editProfileViewModel.isSaved.observe(viewLifecycleOwner, Observer { hasSaved ->
            if (hasSaved) {
                editProfileViewModel.onSaveProfileComplete()
                accountViewModel.loadUserData()
                Snackbar.make(binding.root, "User profile correctly saved", Snackbar.LENGTH_SHORT).show()
                findNavController().popBackStack()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.edit_profile_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save_icon -> {
                if (!editProfileViewModel.isSaving.value!!) {
                    saveProfile()
                } else {
                    Toast.makeText(requireContext(), "Profile is currently being saved", Toast.LENGTH_SHORT).show()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        val inflater: MenuInflater = requireActivity().menuInflater
        inflater.inflate(R.menu.edit_profile_context_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            //TAKE PHOTO FROM GALLERY
            R.id.context_menu_gallery_item -> {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                    ProfileKey.SELECT_IMAGE
                )
                true
            }

            //SHOOT A PHOTO
            R.id.context_menu_camera_item -> {
                dispatchTakePictureIntent()
                true
            }
            else -> false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == ProfileKey.SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                try {
                    val inputStream = data.data?.let { requireActivity().contentResolver.openInputStream(it) }
                    editProfileViewModel.setImg(BitmapFactory.decodeStream(inputStream))
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(activity, "Canceled", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == ProfileKey.REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            editProfileViewModel.setImg(BitmapFactory.decodeFile(editProfileViewModel.currentPhotoPath.value))
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
                    File.createTempFile("PNG_${timeStamp}_", ".png", tempFolder).apply { editProfileViewModel.setCurrentPhotoPath(absolutePath) }
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(requireActivity().applicationContext, "${BuildConfig.APPLICATION_ID}.fileprovider", it)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent,
                        ProfileKey.REQUEST_TAKE_PHOTO
                    )
                }
            }
        }
    }

    private fun saveProfile() {

        val location = autocompleteFragment?.view?.findViewById(R.id.places_autocomplete_search_input) as EditText

        editProfileViewModel.onSaveProfile(User(
            id = editProfileViewModel.user.value?.id,
            name = editProfileViewModel.name.value,
            birthdate = editProfileViewModel.birthdate.value,
            nickname = editProfileViewModel.nickname.value,
            email = editProfileViewModel.email.value,
            phonenumber = editProfileViewModel.phonenumber.value,
            advertisements = editProfileViewModel.user.value?.advertisements,
            totalRatings = editProfileViewModel.user.value?.totalRatings,
            nTransactions = editProfileViewModel.user.value?.nTransactions,
            isSeller = editProfileViewModel.user.value?.isSeller,
            markerLat = marker?.position?.latitude,
            markerLong = marker?.position?.longitude,
            location = location.text.toString()
        ), editProfileViewModel.img.value)

        // remove temp folder and temp files
        tempFolder.listFiles()?.forEach { f -> f.delete() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "$this ~ onCreate")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireActivity().currentFocus?.windowToken, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "$this ~ onDestroy")
    }

    private fun setMarkerFromLatLng(lat: Double, long: Double){
        googleMap?.clear()
        val coordinates = LatLng(lat,long)
        marker = googleMap?.addMarker(
            MarkerOptions()
                .position(coordinates).title("My address")
        )
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 16.0f))
    }

    private fun setLocationFromLatLng(lat: Double, long: Double){
        try {
            val geocoder = Geocoder(requireActivity().applicationContext, Locale.getDefault())
            val addresses = geocoder.getFromLocation(
                lat,
                long,
                1) //1 represent max location result to returned, by documents it recommended 1 to 5
            if(addresses.isNotEmpty()) {
                val address: String =
                    addresses[0].getAddressLine(0) // If any additional address line present than only check with max available address lines by getMaxAddressLineIndex()

                autocompleteFragment?.setText(address)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            Log.e(TAG, "${e.message}")
        }
    }

}
