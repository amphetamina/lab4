package it.polito.phony.lab4.user.account

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.*
import com.google.android.gms.auth.api.Auth
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.StorageException
import it.polito.phony.lab4.user.entity.User
import it.polito.phony.lab4.user.repository.UserRepository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class AccountViewModel(private val userRepository: UserRepository) : ViewModel() {
    enum class AuthenticationState {
        AUTHENTICATED, UNAUTHENTICATED, LOADING, INVALID_AUTHENTICATION
    }

    companion object {
        const val TAG = "AccountViewModel"
        const val LOAD_COMPLETE = "User loading complete"
        const val LOG_OUT = "User correctly logged out"
    }

    private val _id by lazy {
        return@lazy MutableLiveData<String?>()
    }
    val id: LiveData<String?>
        get() = _id

    private val _user by lazy {
        return@lazy MutableLiveData<User?>(null)
    }
    val user: LiveData<User?>
        get() = _user

    private val _img by lazy {
        return@lazy MutableLiveData<Bitmap?>()
    }
    val img: LiveData<Bitmap?>
        get() = _img

    private val _toastmsg by lazy {
        return@lazy MutableLiveData<String?>()
    }
    val toastmsg: LiveData<String?>
        get() = _toastmsg

    fun onShowToastMsgComplete() {
        _toastmsg.value = null
    }

    private val _snackmsg by lazy {
        return@lazy MutableLiveData<String?>()
    }
    val snackmsg: LiveData<String?>
        get() = _snackmsg

    fun onShowSnackMsgComplete() {
        _snackmsg.value = null
    }

    /*
    * trigger logout flow
    * */
    private val _logOut by lazy {
        return@lazy MutableLiveData(false)
    }
    val logOut: LiveData<Boolean>
        get() = _logOut

    private val _isLoading by lazy {
        return@lazy MutableLiveData(false)
    }
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val authListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
        if (firebaseAuth.currentUser != null) {
            _id.value = firebaseAuth.currentUser!!.email
            authenticationState.value = AuthenticationState.LOADING
            loadUserData()
        } else {
            authenticationState.value = AuthenticationState.UNAUTHENTICATED
        }
    }

    val authenticationState by lazy {
        return@lazy MutableLiveData<AuthenticationState>()
    }

    init {
        FirebaseAuth.getInstance().addAuthStateListener(authListener)
        _img.value = userRepository.getProfilePlaceholderImg()
        Log.i(TAG, "init")
    }

    fun loadUserData() {
        viewModelScope.launch(context = IO) {
            _id.value?.let { id ->
                /*
                * if user does not exists, null data will be loaded
                * */
                _isLoading.postValue(true)
                val data = userRepository.getUserData(id)
                val img = userRepository.getUserImg(id)
                _user.postValue(data)
                _img.postValue(img)
                authenticationState.postValue(AuthenticationState.AUTHENTICATED)
                _isLoading.postValue(false)
                // _snackmsg.postValue(LOAD_COMPLETE)
            }
        }
    }

    fun logOut() {
        _logOut.value = true
    }

    fun logOutComplete() {
        _user.value = null
        _img.value = userRepository.getProfilePlaceholderImg()
        _logOut.value = false
        _snackmsg.value = LOG_OUT
    }
}
