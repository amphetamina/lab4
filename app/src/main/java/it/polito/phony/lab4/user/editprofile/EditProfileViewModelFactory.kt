package it.polito.phony.lab4.user.editprofile

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import it.polito.phony.lab4.user.entity.User
import it.polito.phony.lab4.user.repository.UserRepository

class EditProfileViewModelFactory(private val user: User?, private val img: Bitmap?, private val userRepository: UserRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EditProfileViewModel::class.java)) {
            return EditProfileViewModel(user, img, userRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}