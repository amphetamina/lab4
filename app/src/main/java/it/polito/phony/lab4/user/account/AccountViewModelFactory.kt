package it.polito.phony.lab4.user.account

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import it.polito.phony.lab4.user.repository.UserRepository

class AccountViewModelFactory(private val userRepository: UserRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AccountViewModel::class.java)) {
            return AccountViewModel(userRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}