package it.polito.phony.lab4.transaction

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.storage.StorageReference
import it.polito.phony.lab4.databinding.TransactionItemBinding
import it.polito.phony.lab4.entity.AdvertisementRepository

open class TransactionAdapter(
    private val clickListener: OnTransactionClickListener,
    private val addReviewListener: onAddReviewClickListener,
    val activity: Activity,
    options: FirestoreRecyclerOptions<Transaction>,
    val listViewModel: TransactionListViewModel)
    : FirestoreRecyclerAdapter<Transaction, TransactionAdapter.ViewHolder>(options) {

    interface OnTransactionClickListener {
        fun transactionsClickHandler(t: Transaction)
    }

    interface onAddReviewClickListener{
        fun addReviewClickHandler(t: Transaction)
    }

    class ViewHolder(val binding: TransactionItemBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            TransactionItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, t: Transaction) {
        holder.apply {
            if(t.transID.isEmpty()) {
                binding.transactionCard.visibility = View.GONE
            } else {
                binding.transactionCard.visibility = View.VISIBLE
                binding.transaction = t
                binding.listViewModel = listViewModel

                //LoadImage
                val advID = t.advID
                try{
                    val imgRef :StorageReference = AdvertisementRepository.getInstance(activity).getAdvertisementImg(advID)
                    Glide.with(binding.itemImage.context).load(imgRef).into(binding.itemImage)
                } catch (ex: Throwable){
                    Log.i("TransactionAdapter", "$advID img not found")
                }

                //Transaction Card Listener
                binding.transactionCard.setOnClickListener{
                    clickListener.transactionsClickHandler(t)
                }

                if (!t.hasReview){
                    // add review listener
                    binding.addReview.setOnClickListener {
                        addReviewListener.addReviewClickHandler(t)
                    }
                }
            }
        }
    }
}