package it.polito.phony.lab4.request

data class Request(val userId:String, var isBuyer: Boolean)