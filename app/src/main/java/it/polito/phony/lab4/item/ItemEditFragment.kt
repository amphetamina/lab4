package it.polito.phony.lab4.item

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.InputType
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.toBitmap
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import it.polito.phony.lab4.BuildConfig
import it.polito.phony.lab4.R
import it.polito.phony.lab4.databinding.FragmentItemEditBinding
import it.polito.phony.lab4.entity.Advertisement
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.itemlist.ItemListFragment.AdvertisementKey.ADV_PHOTO_PREFIX
import it.polito.phony.lab4.itemlist.ItemListFragment.AdvertisementKey.LATITUDE_KEY
import it.polito.phony.lab4.itemlist.ItemListFragment.AdvertisementKey.LOCATION_KEY
import it.polito.phony.lab4.itemlist.ItemListFragment.AdvertisementKey.LONGITUDE_KEY
import it.polito.phony.lab4.notification.Notification
import it.polito.phony.lab4.user.account.AccountViewModel
import kotlinx.android.synthetic.main.fragment_item_edit.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneOffset
import java.util.*


class ItemEditFragment : Fragment(), AdapterView.OnItemSelectedListener , OnMapReadyCallback {

    private lateinit var binding: FragmentItemEditBinding
    private lateinit var itemEditViewModel: ItemViewModel
    private lateinit var itemViewModelFactory: ItemViewModelFactory
    private val accountViewModel: AccountViewModel by activityViewModels()
    private var isCreating: Boolean? = null
    private var isEdited: Boolean = false
    private lateinit var tempFolder: File
    private var currentPhotoPath: String? = null    //it keeps track of the temporary path of the photo to save.
    private val REQUEST_TAKE_PHOTO  = 1
    private val SELECT_IMAGE = 2
    private var ADV_PHOTO_PATH: String? = null
    private lateinit var advItem : Advertisement
    private var googleMap : GoogleMap? = null
    private var marker : Marker? = null
    private var autocompleteFragment : AutocompleteSupportFragment? = null
    private var restoredLat : Double? = null
    private var restoredLong : Double? = null
    private lateinit var notification : Notification
    private lateinit var advertisementRepository: AdvertisementRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notification = Notification(requireActivity())
    }

    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        advertisementRepository = AdvertisementRepository(requireActivity())
        setHasOptionsMenu(true)
        val img : Bitmap = resources.getDrawable(R.drawable.add_photo, null).toBitmap()
        advItem = Advertisement()
        //arguments passed from ItemListFragment. Two cases:
        //1) if edit -> id = "uuid"
        //2) if add -> id = null
        arguments?.let {
            advItem = if ((it.get("advertisement") as Advertisement?) != null) it.get("advertisement") as Advertisement else Advertisement()
        }
        tempFolder = File(activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString()+"/temp")
        tempFolder.mkdir()
        if (advItem.id.isNotEmpty()){
            ADV_PHOTO_PATH = "$ADV_PHOTO_PREFIX$id.png"
        }

        //if id is null or empty -> isCreating = true; else -> isCreating = false
        isCreating = advItem.id.isEmpty()
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_edit, container, false)
        itemViewModelFactory = ItemViewModelFactory(advItem, img, accountViewModel.user.value!!.id!!, AdvertisementRepository.getInstance(requireActivity()))
        itemEditViewModel = ViewModelProvider(this, itemViewModelFactory).get(ItemViewModel::class.java)
        binding.itemViewModel = itemEditViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        //CATEGORY SPINNER
        val spinner: Spinner = binding.root.findViewById(R.id.itemCategory_edit_spinner)
        spinner.onItemSelectedListener = this
        ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.category_arrays,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        val categories = this.requireContext().resources.getStringArray(R.array.category_arrays)
        if (itemEditViewModel.advertisement.value!!.category.isNotEmpty()){
            val idx = categories.indexOf(itemEditViewModel.advertisement.value!!.category)
            spinner.setSelection(idx)
        }
        setSubCategory(spinner.selectedItem.toString())

        // STATUS SPINNER
        val statusSpinner: Spinner = binding.root.findViewById(R.id.status_spinner)
        statusSpinner.onItemSelectedListener = this
        ArrayAdapter.createFromResource(                    // Create an ArrayAdapter using the string array and a default spinner layout
            this.requireContext(),
            R.array.status_arrays,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)   // Specify the layout to use when the list of choices appears
            statusSpinner.adapter = adapter                       // Apply the adapter to the spinner
        }
        val status = this.requireContext().resources.getStringArray(R.array.status_arrays)
        if (itemEditViewModel.advertisement.value!!.status.isNotEmpty()){
            val idx = status.indexOf(itemEditViewModel.advertisement.value!!.status)
            statusSpinner.setSelection(idx)
        }

        //EXPIRATION_DATE DATE PICKER, DO NOT OPEN KEYBOARD
        binding.itemExpDateEdit.inputType = InputType.TYPE_NULL
        binding.itemExpDateEdit.setOnClickListener {
            val cldr = Calendar.getInstance()
            var day = cldr[Calendar.DAY_OF_MONTH]
            var month = cldr[Calendar.MONTH]
            var year = cldr[Calendar.YEAR]

            if (binding.itemExpDateEdit.text != null && !binding.itemExpDateEdit.text.isNullOrBlank()){
                val dateComps = binding.itemExpDateEdit.text.split('-')
                val prevYear = dateComps[0].toIntOrNull()
                val prevMonth = dateComps[1].toIntOrNull()
                val prevDay = dateComps[2].toIntOrNull()

                if (prevDay != null && prevMonth != null && prevYear != null){
                    day = prevDay
                    month = prevMonth - 1
                    year = prevYear
                }
            }

            //DATE PICKER DIALOG
            val picker = DatePickerDialog(
                this.requireContext(),
                OnDateSetListener { _, selectedYear, monthOfYear, dayOfMonth ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        val dateStr = LocalDate.of(selectedYear, monthOfYear+1, dayOfMonth).toString()
                        binding.itemExpDateEdit.setText(dateStr)
                    } else {
                        val MoY = if(monthOfYear < 9) "0"+(monthOfYear+1).toString() else (monthOfYear+1).toString()
                        val DoM = if(dayOfMonth < 10) "0"+(dayOfMonth).toString() else (dayOfMonth).toString()
                        val dateString = "$selectedYear-$MoY-$DoM"
                        binding.itemExpDateEdit.setText(dateString)
                    }
                },
                year,
                month,
                day
            )
            picker.show()
        }

        //CAMERA BUTTON LISTENERS
        val cameraButton : ImageView = binding.root.findViewById(R.id.editAdvertisement)
        registerForContextMenu(cameraButton)
        registerForContextMenu(binding.itemImageView)
        cameraButton.setOnLongClickListener{
                view -> view.showContextMenu()
        }
        cameraButton.setOnClickListener{view ->
            view.showContextMenu()
        }
        binding.itemImageView.setOnLongClickListener { view -> view.showContextMenu() }
        binding.itemImageView.setOnClickListener { view -> view.showContextMenu() }


        //MAP FRAGMENT
        val mapFragment : SupportMapFragment = childFragmentManager.findFragmentById(R.id.mainMap) as SupportMapFragment
        mapFragment.getMapAsync(this@ItemEditFragment)

        //ENABLE VERTICAL SCROLL ON MAP
        val mainScrollView : ScrollView =  binding.root.findViewById(R.id.main_scrollview)
        val transparentImageView : ImageView = binding.root.findViewById(R.id.transparent_image)
        transparentImageView.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(true)      // Disallow ScrollView to intercept touch events
                    return@setOnTouchListener false                                             //Disable touch on transparent view
                }
                MotionEvent.ACTION_UP -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(false)     // Allow ScrollView to intercept touch events
                    return@setOnTouchListener true
                }
                MotionEvent.ACTION_MOVE -> {
                    mainScrollView.requestDisallowInterceptTouchEvent(true)
                    return@setOnTouchListener false
                }
                else -> return@setOnTouchListener true
            }
        }

        if (!Places.isInitialized()) {
            Places.initialize(requireActivity().applicationContext, getString(R.string.google_maps_key))
        }
        val placeFields : List<Place.Field> = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)
        autocompleteFragment = childFragmentManager.findFragmentById(R.id.autocomplete_fragment) as? AutocompleteSupportFragment
        autocompleteFragment?.setPlaceFields(placeFields)

        autocompleteFragment?.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                if(place.latLng != null) {
                    setMarkerFromLatLng(place.latLng!!.latitude, place.latLng!!.longitude)
                }
            }

            override fun onError(status: Status) {
                Log.i("kkk", "An error occurred: $status")
            }
        })

        itemEditViewModel.isSaved.observe(viewLifecycleOwner, androidx.lifecycle.Observer { hasSaved ->
            if (hasSaved) {
                tempFolder.listFiles()?.forEach { f -> f.delete() }

                //SEND ADV SOLD NOTIFICATION TO ALL THE INTERESTED USERS (INCLUDED THE ONE TO WHICH IT HAS BEEN SOLD)
                val advId = advItem.id
                val advTitle = itemTitle_edit.text.toString()
                if(status_spinner.selectedItem.toString() == "Sold"  &&  itemEditViewModel.advertisement.value!!.status != status_spinner.selectedItem.toString()) {
                    notification.send("/topics/advSold$advId",
                        "Item sold",
                       "The item $advTitle has been sold",
                        "advertisement_id",
                        advId,
                        this.toString(),
                        R.id.itemDetailsFragment)
                }

                //SEND ADV NO MORE ON SALE NOTIFICATION (Blocked) TO ALL THE INTERESTED USERS
                else if(status_spinner.selectedItem.toString() == "Blocked"  &&  itemEditViewModel.advertisement.value!!.status != status_spinner.selectedItem.toString()) {
                    notification.send("/topics/advNoMoreOnSale$advId",
                       "Item no more on sale",
                        "The item $advTitle is no more on sale",
                        "advertisement_id",
                        advId,
                        this.toString(),
                        R.id.itemDetailsFragment)
                }

                else{
                    notification.send("/topics/advModified$advId",
                        "Item modified",
                        "The item $advTitle has been modified",
                        "advertisement_id",
                        advId,
                        this.toString(),
                        R.id.itemDetailsFragment)
                }

                Toast.makeText(requireContext(), "Advertisement successfully saved", Toast.LENGTH_SHORT).show()

                findNavController().navigate(R.id.action_itemEditFragment_to_itemListFragment)
            }
        })

        itemEditViewModel.toastMsg.observe(viewLifecycleOwner, androidx.lifecycle.Observer { msg ->
            msg?.let {
                Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
            }
        })

        return binding.root
    }

    override fun onMapReady(myMap: GoogleMap?) {
        googleMap = myMap
        try {
            val location = autocompleteFragment?.view?.findViewById(R.id.places_autocomplete_search_input) as EditText
            if(location.text.toString().isEmpty() || location.text.toString().isBlank()) {
                if (itemEditViewModel.advertisement.value?.markerLat != null && itemEditViewModel.advertisement.value?.markerLong != null) {
                    setMarkerFromLatLng(
                        itemEditViewModel.advertisement.value?.markerLat!!,
                        itemEditViewModel.advertisement.value?.markerLong!!
                    )
                    setLocationFromLatLng(
                        itemEditViewModel.advertisement.value?.markerLat!!,
                        itemEditViewModel.advertisement.value?.markerLong!!
                    )
                }
            }

            //TO HANDLE DEVICE ROTATION
            else{
                restoredLat?.let {
                    restoredLong?.let { it1 ->
                        setMarkerFromLatLng(
                            it,
                            it1
                        )
                    }
                }
            }

            myMap?.setOnMapClickListener {
                setMarkerFromLatLng(
                    it.latitude,
                    it.longitude
                )
                setLocationFromLatLng(
                    it.latitude,
                    it.longitude
                )
            }
        } catch (e: Exception){
                e.printStackTrace()
        }
    }

    //FRAGMENT XML TAG DOESN'T SUPPORT DATA BINDING SI I USE THE FOLLOWING 2 METHODS TO SAVE AND RESTORE THE STATE DURNG ROTATION
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val location = autocompleteFragment?.view?.findViewById(R.id.places_autocomplete_search_input) as EditText
        outState.putString(LOCATION_KEY, location.text.toString())
        if (marker?.position?.latitude != null) {
            outState.putDouble(LATITUDE_KEY, marker?.position?.latitude!!)
            outState.putDouble(LONGITUDE_KEY, marker?.position?.longitude!!)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            autocompleteFragment?.setText(savedInstanceState.getString(LOCATION_KEY))
            restoredLat = savedInstanceState.getDouble(LATITUDE_KEY)
            restoredLong = savedInstanceState.getDouble(LONGITUDE_KEY)
        }
    }

    //MENU SAVE ICON
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.edit_advertisement_menu, menu)
    }

    //SAVE
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save_icon -> {
                if (!itemEditViewModel.isSaving.value!!) {
                    saveAdvertisement()
                } else {
                    Toast.makeText(requireContext(), "Advertisement currently being saved", Toast.LENGTH_SHORT).show()
                }

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //SAVE FUNCTION, return the id
    private fun saveAdvertisement(): Boolean {
        // check validity
        var msg = itemEditViewModel.checkTitle(itemTitle_edit.text.toString())
        if (msg != null) {
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            return false
        }
        msg = itemEditViewModel.checkDescription(itemDescription_edit.text.toString())
        if (msg != null){
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            return false
        }
        val location = autocompleteFragment?.view?.findViewById(R.id.places_autocomplete_search_input) as EditText
        msg = itemEditViewModel.checkLocation(location.text.toString())
        if (msg != null){
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            return false
        }
        msg = itemEditViewModel.checkPrice(itemPrice_edit.text.toString())
        if (msg != null){
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            return false
        }
        msg = itemEditViewModel.checkExpDate(itemExpDate_edit.text.toString())
        if (msg != null){
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            return false
        }
        if (marker == null){
            Toast.makeText(activity, "Please select a location on the map", Toast.LENGTH_SHORT).show()
            return false
        }
        if ((isCreating!! && !isEdited) && itemEditViewModel.photoPath.value.isNullOrEmpty()){
            Toast.makeText(activity, "Please insert an image", Toast.LENGTH_SHORT).show()
            return false
        }

        //if we are creating it the id is empty. Infact if we are editing it we have already received id and set th ADV_PHOTO_PATH previously
        var id = advItem.id
        if(id.isEmpty()){
            id = UUID.randomUUID().toString()
            ADV_PHOTO_PATH = "$ADV_PHOTO_PREFIX$id.png"

            //SUBSCRIBE TO INTERESTED USERS TOPIC
            notification.subscribe("/topics/newInterestedUsers$id", this.toString())
        }

        val itemDate: Date
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val zdt =  LocalDate.parse(itemExpDate_edit.text.toString()).atStartOfDay()
            itemDate = Date.from(zdt.toInstant(ZoneOffset.UTC))
        } else {
            itemDate = SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(itemExpDate_edit.text.toString())!!
        }

        val newAdv = Advertisement("",
            accountViewModel.user.value!!.id,
            itemTitle_edit.text.toString(),
            itemDescription_edit.text.toString(),
            location.text.toString(),
            itemPrice_edit.text.toString().toDouble(),
            itemCategory_edit_spinner.selectedItem.toString(),
            itemSubCategory_edit_spinner.selectedItem.toString(),
            itemDate,
            Date(),
            status_spinner.selectedItem.toString(),
            ADV_PHOTO_PATH!!,
            listOf(),   //TODO
            marker?.position?.latitude,
            marker?.position?.longitude
        )

        itemEditViewModel.saveAdvertisement(id, accountViewModel.user.value!!.id!!, advItem, newAdv)

        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {
            autocompleteFragment?.setText(savedInstanceState.getString(LOCATION_KEY))
            restoredLat = savedInstanceState.getDouble(LATITUDE_KEY)
            restoredLong = savedInstanceState.getDouble(LONGITUDE_KEY)
        }

        if(!advItem.id.isEmpty())
            itemEditViewModel.loadEditImg(advItem.id)
    }

    //CHANGE PHOTO
    override fun onCreateContextMenu(menu: ContextMenu, v: View,
                                     menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity?.menuInflater?.inflate(R.menu.edit_profile_context_menu, menu)
    }

    //CAMERA
    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            //TAKE PHOTO FROM GALLERY
            R.id.context_menu_gallery_item -> {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE)
                true
            }

            //SHOOT A PHOTO
            R.id.context_menu_camera_item -> {
                dispatchTakePictureIntent()
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            activity?.packageManager?.let {
                takePictureIntent.resolveActivity(it)?.also {
                    // Create the File where the photo should go
                    val photoFile: File? = try {
                        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(
                            Date()
                        )
                        File.createTempFile("PNG_${timeStamp}_", ".png", tempFolder).apply { currentPhotoPath = absolutePath }
                    } catch (ex: IOException) {
                        // Error occurred while creating the File
                        null
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(requireActivity().applicationContext, "${BuildConfig.APPLICATION_ID}.fileprovider", it)
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //SELECT IMAGE
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                try {
                    val inputStream = data.data?.let { activity?.contentResolver?.openInputStream(it) }
                    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
                    val file = File.createTempFile("PNG_${timeStamp}_",".png", tempFolder)
                    if (inputStream != null) {
                        file.outputStream().use { fileOut ->
                            inputStream.copyTo(fileOut) }
                    }
                    currentPhotoPath = file.path
                    itemEditViewModel.setCurrentPhotoPath(file.path)
                    itemEditViewModel.setImg(BitmapFactory.decodeFile(file.path), binding.itemImageView.width, binding.itemImageView.height)
                    isEdited = true

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(activity?.applicationContext, "Canceled", Toast.LENGTH_SHORT).show()
            }
        }

        //SHOOT A PHOTO
        else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == AppCompatActivity.RESULT_OK) {
            //itemImageView.setImageBitmap(BitmapFactory.decodeFile(currentPhotoPath))
            itemEditViewModel.setCurrentPhotoPath(currentPhotoPath!!)
            isEdited = true
            itemEditViewModel.setImg(BitmapFactory.decodeFile(itemEditViewModel.photoPath.value), binding.itemImageView.width, binding.itemImageView.height)
        }

        //CANCEL OPERATION
        else{
            if (!currentPhotoPath.isNullOrEmpty()) {
                val file: File? = try {
                    File(currentPhotoPath!!)
                } catch (ex: IOException) {
                    null
                }
                file?.also {
                    file.delete()
                }
                currentPhotoPath = null
            }
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
        val selectedItem = parent!!.getItemAtPosition(position)
        setSubCategory(selectedItem.toString())
    }

    private fun setSubCategory(selectedCategory: String){
        var selectedArray = R.array.arts_and_crafts_arrays
        when(selectedCategory){
            "Arts & Crafts" -> {
                selectedArray = R.array.arts_and_crafts_arrays
            }
            "Sports & Hobby" -> {
                selectedArray = R.array.sports_and_hobby_arrays
            }
            "Baby" -> {
                selectedArray = R.array.baby_arrays
            }
            "Women fashion" -> {
                selectedArray = R.array.women_fashion_arrays
            }
            "Men fashion" -> {
                selectedArray = R.array.men_fashion_arrays
            }
            "Electronics" -> {
                selectedArray = R.array.electronics_arrays
            }
            "Games & Videogames" -> {
                selectedArray = R.array.games_and_videogames_arrays
            }
            "Automotive" -> {
                selectedArray = R.array.automotive_arrays
            }
        }
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this.requireContext(),
            selectedArray,
            android.R.layout.simple_spinner_item
        ).also { adapter ->                                         // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.itemSubCategoryEditSpinner.adapter = adapter    // Apply the adapter to the spinner
            if (binding.itemCategoryEditSpinner.selectedItem.toString() == itemEditViewModel.advertisement.value!!.category){
                val categories = this.requireContext().getResources().getStringArray(selectedArray)
                if (!itemEditViewModel.advertisement.value!!.subcategory.isEmpty()){
                    val idx = categories.indexOf(itemEditViewModel.advertisement.value!!.subcategory)
                    binding.itemSubCategoryEditSpinner.setSelection(idx)
                }
            }

        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }

    private fun setMarkerFromLatLng(lat: Double, long: Double){
        googleMap?.clear()
        val coordinates = LatLng(lat,long)
        marker = googleMap?.addMarker(
            MarkerOptions()
                .position(coordinates).title("Item on sale")
        )
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 16.0f))
    }

    private fun setLocationFromLatLng(lat: Double, long: Double){
        try {
            val geocoder = Geocoder(requireActivity().applicationContext, Locale.getDefault())
            val addresses = geocoder.getFromLocation(
                lat,
                long,
                1)                              //1 represent max location result to returned, by documents it recommended 1 to 5

            if(addresses.isNotEmpty()) {
                val address: String =
                    addresses[0].getAddressLine(0)  // If any additional address line present than only check with max available address lines by getMaxAddressLineIndex()
                autocompleteFragment?.setText(address)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            Log.e("ItemEditFragment", "${e.message}")
        }
    }
}