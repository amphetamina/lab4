package it.polito.phony.lab4.user.repository

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.widget.Toast
import androidx.core.graphics.drawable.toBitmap
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FieldValue.arrayRemove
import com.google.firebase.firestore.FieldValue.arrayUnion
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.getField
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageException
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import it.polito.phony.lab4.R
import it.polito.phony.lab4.entity.AdvertisementRepository
import it.polito.phony.lab4.repository.FirebaseConstants
import it.polito.phony.lab4.transaction.Transaction
import it.polito.phony.lab4.user.entity.User
import it.polito.phony.lab4.user.entity.UserRating
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt
import kotlin.math.roundToLong

class UserRepository(private val activity: Activity) {

    companion object {
        const val TAG = "UserRepository"
        const val COLLECTION = "users"
        const val RATING_COLLECTION = "transactions"
        const val FOLDER = "profile_imgs"
        const val EXTENSION = ".png"
        const val WAIT = 10L

        private var instance: UserRepository? = null
        fun getInstance(activity: Activity) =
            instance ?: synchronized(this) {
                instance ?: UserRepository(activity).also { instance = it }
            }
    }

    private val db = FirebaseFirestore.getInstance()
    private val storage = FirebaseStorage.getInstance().reference

    fun getUserData(id: String): User? {
        return try {
            val dataTask = db.collection(COLLECTION).document(id).get()
            val snapshot = Tasks.await(dataTask)
            snapshot.toObject(User::class.java)
        } catch (e: Throwable) {
            e.printStackTrace()
            null
        }
    }

    fun getUserImg(id: String): Bitmap? {
        return try {
            val reference = storage.child(FOLDER).child(id + EXTENSION)
            if (reference.metadata.exception == null) {
                val snapshot = Tasks.await(reference.stream)
                BitmapFactory.decodeStream(snapshot.stream)
            } else {
                getProfilePlaceholderImg()
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            getProfilePlaceholderImg()
        }
    }

    fun getUserImgReference(id: String): StorageReference {
        return storage.child(FOLDER).child(id+EXTENSION)
    }

    fun setUserData(id: String, data: User): Boolean {
        return try {
            val saveTask = db.collection(COLLECTION).document(id).set(data)
            Tasks.await(saveTask)
            true
        } catch (e: Throwable) {
            e.printStackTrace()
            Log.e(TAG, "${e.message}")
            false
        }
    }

    fun setUserImg(id: String, img: Bitmap?): Boolean {
        img?.let { bitmap ->
            try {
                val ref = storage.child(FOLDER).child(id + EXTENSION)
                val byteArrayOutputStream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
                val data = byteArrayOutputStream.toByteArray()
                val uploadTask = ref.putBytes(data)
                Tasks.await(uploadTask)
                return true
            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
        return false
    }

    //ADD AdvertisementInterestedTo IN USERS.advertisementInterestedTo
    fun addAdvertismentInterestedTo (userId: String, advId: String): Task<Void> {
        val addAdvertisement: MutableMap<String, Any> = HashMap()
        addAdvertisement["advertisementsInterestedTo"] = arrayUnion(advId)
        return db.collection("users").document(userId).update(addAdvertisement)
    }

    //REMOVE AdvertisementInterestedTo FROM USERS.advertisementInterestedTo
    fun removeAdvertisementInterestedTo(userId: String, advId: String){
        val addAdvertisement: MutableMap<String, Any> = HashMap()
        addAdvertisement["advertisementsInterestedTo"] = arrayRemove(advId)
        db.collection("users").document(userId).update(addAdvertisement)
    }


    //ADD ADVERTISEMENT IN USERS.advertisement
    fun addAdvertisment (userId: String, advId: String): Task<Void> {
        val addAdvertisement: MutableMap<String, Any> = HashMap()
        addAdvertisement["advertisements"] = arrayUnion(advId)
        return db.collection("users").document(userId).update(addAdvertisement)
    }

    //REMOVE ADVERTISEMENT IN USERS.advertisement
    fun removeAdvertisement(userId: String, advId: String){
        val addAdvertisement: MutableMap<String, Any> = HashMap()
        addAdvertisement["advertisements"] = arrayRemove(advId)
        db.collection("users").document(userId).update(addAdvertisement)
    }

    fun updateRatings(userID: String, amount: Float, isNegative: Boolean) {
        var rating = 0F
        val nTrans = 0
        var nothingDone: Boolean
        db.collection(COLLECTION)
            .document(userID)
            .get()
            .addOnSuccessListener(activity) {
                val r: Float? = it.getField(FirebaseConstants.RATING_FIELD)
                val nt: Int? = it.getField(FirebaseConstants.N_TRANSACTIONS_FIELD)
                if ((r?.isNaN()!! || r == 0F) && (nt == 0 || nt == null)) {
                    //nothing to update, create now using 'rating' variable
                    rating += amount
                    //increment nTransaction field
                    nTrans.inc()
                    //I have done something
                    nothingDone = false

                } else {
                    //user already have history as a seller -> r != null && nt != null
                    //so the rating could be due of a remove of a transaction, or one extra

                    if (isNegative) {
                        rating.plus(r).minus(amount)
                        nt?.let { value -> nTrans.plus(value).dec() }
                        nothingDone = false
                    } else {
                        rating.plus(r).plus(amount)
                        nt?.let { value -> nTrans.plus(value).inc() }
                        nothingDone = false
                    }
                }

                if (!nothingDone) {
                    db.collection(COLLECTION).document(userID).update(FirebaseConstants.RATING_FIELD, rating)
                    db.collection(COLLECTION).document(userID).update(FirebaseConstants.N_TRANSACTIONS_FIELD, nTrans)
                }
            }
            .addOnFailureListener(activity) {
                Log.d("TAG", "Errore nell'update del rating")
            }
    }

    fun getUserTotalRating(userID: String): Float? {
        val totalRatings : Float? = 0F
        db.collection(COLLECTION).document(userID)
            .get()
            .addOnSuccessListener(activity) {
                val r: Float? = it.getField(FirebaseConstants.RATING_FIELD)
                r?.let {value -> totalRatings?.plus(value)}
            }

        return totalRatings

    }

    fun getUserTransTotNumber(userID: String): Float? {
        val nTrans: Float? = 0F
        db.collection(COLLECTION).document(userID)
            .get()
            .addOnSuccessListener(activity) {
                val nt: Int? = it.getField(FirebaseConstants.N_TRANSACTIONS_FIELD)
                nt?.let {value -> nTrans?.plus(value)}
            }
        return nTrans
    }

    fun getProfilePlaceholderImg(): Bitmap {
        return activity.resources.getDrawable(R.drawable.doge, activity.theme).toBitmap()
    }

    fun isSeller(userID: String) {
        db.collection(COLLECTION).document(userID)
            .get()
            .addOnSuccessListener(activity) {
                val isSeller : Unit? = it.getField(FirebaseConstants.SELLER_FIELD)
                if(isSeller != null) {
                    if(isSeller.equals(true))
                        return@addOnSuccessListener isSeller
                    else
                        return@addOnSuccessListener isSeller
                }
            }
    }

    fun getAvgRating(_sellerID: String) : UserRating? {
        return try {
            val dataTask = db.collection(RATING_COLLECTION).whereEqualTo("sellerID", _sellerID).whereEqualTo("hasReview", true).get()
            val snapshot = Tasks.await(dataTask)
            val reviews = snapshot.toObjects(Transaction::class.java)
            val userRating = UserRating()
            var avg = 0f
            if (reviews.size > 0) {
                var ratingSum = 0f
                val count = reviews.size
                reviews.forEach{
                    ratingSum += it.rating!!
                }
                avg = ratingSum / count
                val number3digits:Double = (avg * 1000.0).roundToLong() / 1000.0
                val number2digits:Double = (number3digits * 100.0).roundToInt() / 100.0
                val solution:Double = (number2digits * 10.0).roundToInt() / 10.0
                userRating.rating = solution.toFloat()
                userRating.hasReviews = true
            }
            else{
                userRating.hasReviews = false
                userRating.rating = 0f
            }
            userRating
        } catch (e: Throwable) {
            e.printStackTrace()
            null
        }
    }


}