package it.polito.phony.lab4.transaction

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.storage.StorageReference
import it.polito.phony.lab4.databinding.SoldItemBinding
import it.polito.phony.lab4.entity.AdvertisementRepository

open class SoldItemAdapter (
    private val clickListener: OnTransactionClickListener,
    val activity: Activity,
    options: FirestoreRecyclerOptions<Transaction>,
    val listViewModel: TransactionListViewModel)
    : FirestoreRecyclerAdapter<Transaction, SoldItemAdapter.ViewHolder>(options) {

    interface OnTransactionClickListener {
        fun transactionsClickHandler(t: Transaction)
    }

    class ViewHolder(val binding: SoldItemBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            SoldItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, t: Transaction) {
        holder.apply {
            if(t.transID.isEmpty()) {
                binding.soldItemCard.visibility = View.GONE
            } else {
                binding.soldItemCard.visibility = View.VISIBLE
                binding.transaction = t
                binding.listViewModel = listViewModel

                //LoadImage
                val advID = t.advID
                try {
                    val imgRef: StorageReference =
                        AdvertisementRepository.getInstance(activity).getAdvertisementImg(advID)
                    Glide.with(binding.soldItemImage.context).load(imgRef)
                        .into(binding.soldItemImage)
                } catch (ex: Throwable) {
                    Log.i("SoldItemAdapter", "$advID img not found")
                }

                //Transaction Card Listener
                binding.soldItemCard.setOnClickListener {
                    clickListener.transactionsClickHandler(t)
                }
            }
        }
    }
}