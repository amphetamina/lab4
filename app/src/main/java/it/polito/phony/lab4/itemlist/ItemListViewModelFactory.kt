package it.polito.phony.lab4.itemlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import it.polito.phony.lab4.item.ItemViewModel

class ItemListViewModelFactory(private val isOwner: Boolean): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ItemListViewModel::class.java)) {
            return ItemListViewModel(isOwner) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}