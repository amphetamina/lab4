package it.polito.phony.lab4.entity

import android.app.Activity
import android.graphics.Bitmap
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.*
import com.google.firebase.firestore.FieldValue.arrayRemove
import com.google.firebase.firestore.FieldValue.arrayUnion
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import it.polito.phony.lab4.user.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import kotlin.collections.HashMap


class AdvertisementRepository(private val activity: Activity) {

    companion object {
        const val TAG = "AdvetisementRepository"
        const val COLLECTION = "advertisements"
        const val FOLDER = "ad_imgs"
        const val EXTENSION = ".jpg"

        private var instance: AdvertisementRepository? = null
        fun getInstance(activity: Activity) =
            instance ?: synchronized(this) {
                instance ?: AdvertisementRepository(activity).also { instance = it }
            }
    }

    private val db = FirebaseFirestore.getInstance()
    private val storage = FirebaseStorage.getInstance().reference

    fun getUserAdvertisements(userId: String): Query{
        return db.collection(COLLECTION).whereEqualTo(Advertisement.FIELD_USER, userId).orderBy(Advertisement.FIELD_CREATION_DATE, Query.Direction.DESCENDING)
    }

    fun getOnSaleAdvertisements() : Query {
        return db.collection(COLLECTION).whereEqualTo(Advertisement.FIELD_STATUS, "Available")
            //.whereGreaterThanOrEqualTo(Advertisement.FIELD_EXPIRATION_DATE, Date())
            //.orderBy(Advertisement.FIELD_EXPIRATION_DATE, Query.Direction.ASCENDING)
    }

    fun getUserInterestAdvertisements(userId: String) : Query{
        return db.collection(COLLECTION)
            .whereArrayContains(Advertisement.FIELD_INTERESTED_USERS, userId)
            .whereEqualTo(Advertisement.FIELD_STATUS, Advertisement.Status.Available.toString())
            .orderBy(Advertisement.FIELD_CREATION_DATE, Query.Direction.DESCENDING)
    }

    suspend fun addAdvertisement(userId: String, advId: String, newAdv: Advertisement): Task<Void> {

        withContext(Dispatchers.IO) {
            try {
                //ADD ADVERTISEMENT IN USERS.advertisement
                val task = UserRepository.getInstance(activity).addAdvertisment(userId,advId)
                Tasks.await(task)
            } catch (e: Throwable) {
                e.printStackTrace()
                Log.e(TAG, "${e.message}")
                throw e
            }

        }


        //ADD ADVERTISEMENT IN ADVERTISEMENT
        return db.collection(COLLECTION).document(advId).set(newAdv)
    }

    fun updateAdvertisementStatus(advId: String, status: Advertisement.Status){
        db.collection(COLLECTION).document(advId).update(Advertisement.FIELD_STATUS, status.toString())
    }

    suspend fun updateAdvertisement(userId: String, advId: String, newAdv: Advertisement): Task<Void> {
        withContext(Dispatchers.IO) {
            try {
                //ADD ADVERTISEMENT IN USERS.advertisement
                val task = UserRepository.getInstance(activity).addAdvertisment(userId,advId)
                Tasks.await(task)
            } catch (e: Throwable) {
                e.printStackTrace()
                Log.e(TAG, "${e.message}")
                throw e
            }
        }


        //ADD ADVERTISEMENT IN ADVERTISEMENT
        return db.collection(COLLECTION).document(advId).update("userid",userId,
            "title",newAdv.title,
            "description",newAdv.description,
            "location", newAdv.location,
            "price",newAdv.price,
            "category", newAdv.category,
            "subcategory", newAdv.subcategory,
            "expirationdate",newAdv.expirationdate,
            "creationdate", newAdv.creationdate,
            "status", newAdv.status,
            "img",newAdv.img,
            "markerLat", newAdv.markerLat,
            "markerLong", newAdv.markerLong)



    }


    fun removeAdvertisement(userId: String, advId: String){

        //REMOVE ADVERTISEMENT IN ADVERTISEMENT
        db.collection(COLLECTION).document(advId).delete()

        //REMOVE ADVERTISEMENT IN USERS.advertisement
        UserRepository.getInstance(activity).removeAdvertisement(userId,advId)

        //REMOVE IMAGE
        this.deleteAdvertisementImg(advId)
    }

    fun addInterestedUser(advertisementId: String, userId: String){

        //ADD INTERESTED USERS IN ADVERTISEMENT.interestedUsers
        val addUser: MutableMap<String, Any> = HashMap()
        addUser["interestedUsers"] = arrayUnion(userId)
        db.collection(COLLECTION).document(advertisementId).update(addUser)

        //ADD ADVERTISEMENT IN USERS.advertisementInterestedTo
        UserRepository.getInstance(this.activity).addAdvertismentInterestedTo(userId,advertisementId)
    }

    fun removeInterestedUser(advertisementId: String, userId:String){

        //REMOVE INTERESTED USERS IN ADVERTISEMENT.interestedUsers
        val removeUser: MutableMap<String, Any> = HashMap()
        removeUser["interestedUsers"] = arrayRemove(userId)
        db.collection(COLLECTION).document(advertisementId).update(removeUser)

        //REMOVE ADVERTISEMENT IN USERS.advertisementInterestedTo
        UserRepository.getInstance(this.activity).removeAdvertisementInterestedTo(userId,advertisementId)
    }


    fun removeAllInterestedUser(advertisementId: String){

        //REMOVE ALL INTERESTED USERS IN ADVERTISEMENT.interestedUsers
        val removeUser: MutableMap<String, Any> = HashMap()
        removeUser["interestedUsers"] = arrayRemove()
        db.collection(COLLECTION).document(advertisementId).update(removeUser)

    }

    fun getAdvertisementById(id: String): DocumentReference {
        return db.collection(COLLECTION).document(id)
    }

    fun getAdvertisemntByIdAwait(id: String) : Advertisement?{
        return try {
            val getAdvTask = db.collection(COLLECTION).document(id).get()
            val snapshot = Tasks.await(getAdvTask)
            snapshot.toObject(Advertisement::class.java)
        } catch (e: Throwable) {
            e.printStackTrace()
            null
        }
    }

    fun getAdvertisementImg(id: String): StorageReference {
        return storage.child(FOLDER).child(id+EXTENSION)
    }

    fun deleteAdvertisementImg(id: String) {
        storage.child(FOLDER).child(id+EXTENSION).delete()
    }

    fun setAdvertisementImg(id: String, img: Bitmap): UploadTask {
        val ref = storage.child(FOLDER).child(id + EXTENSION)
        val byteArrayOutputStream = ByteArrayOutputStream()
        img.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val data = byteArrayOutputStream.toByteArray()
        return ref.putBytes(data)
    }

    fun getAdPlaceholderImg(): Bitmap {
        // todo change returned bitmap
        return Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565)
    }
}