<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>
        <import type="android.view.View"/>
        <variable
            name="showProfileViewModel"
            type="it.polito.phony.lab4.user.profile.ShowProfileViewModel" />
    </data>

    <androidx.swiperefreshlayout.widget.SwipeRefreshLayout
        android:id="@+id/refresh_layout"
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <ScrollView
            android:id="@+id/main_scrollview"
            android:layout_width="match_parent"
            android:layout_height="match_parent">

            <LinearLayout
                android:id="@+id/main_layout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                android:paddingStart="16dp"
                android:paddingEnd="16dp"
                android:paddingBottom="16dp"
                android:weightSum="3">

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:id="@+id/profile_image_layout"
                    android:layout_width="match_parent"
                    android:layout_height="0dp"
                    android:layout_weight="1">

                    <de.hdodenhof.circleimageview.CircleImageView
                        android:id="@+id/user_image"
                        android:layout_width="0dp"
                        android:layout_height="0dp"
                        android:layout_marginTop="10dp"
                        android:contentDescription="@string/user_image"
                        android:src="@{showProfileViewModel.img}"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintDimensionRatio="1:1"
                        app:layout_constraintEnd_toStartOf="@+id/guideline2"
                        app:layout_constraintStart_toStartOf="@+id/guideline"
                        app:layout_constraintTop_toTopOf="parent" />

                    <androidx.constraintlayout.widget.Guideline
                        android:id="@+id/guideline"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:orientation="vertical"
                        app:layout_constraintGuide_percent="0.25" />

                    <androidx.constraintlayout.widget.Guideline
                        android:id="@+id/guideline2"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:orientation="vertical"
                        app:layout_constraintGuide_percent="0.75" />

                </androidx.constraintlayout.widget.ConstraintLayout>

                <LinearLayout
                    android:id="@+id/user_info_layout"
                    android:layout_width="match_parent"
                    android:layout_height="0dp"
                    android:layout_weight="2"
                    android:orientation="vertical">

                    <TextView
                        android:id="@+id/user_rating_label"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginBottom="5dp"
                        android:text="@string/user_rating_label"
                        android:textColor="@color/colorAccent"
                        android:visibility="@{showProfileViewModel.rating.hasReviews ? View.VISIBLE : View.GONE}" />

                    <LinearLayout
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:orientation="horizontal"
                        android:visibility="@{showProfileViewModel.rating.hasReviews ? View.VISIBLE : View.GONE}">

                        <RatingBar
                            android:id="@+id/userRatingBar"
                            style="@style/Widget.AppCompat.RatingBar.Small"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:isIndicator="true"
                            android:numStars="5"
                            android:rating="@{showProfileViewModel.rating.rating}" />

                        <TextView
                            android:id="@+id/rating_text_view"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_marginStart="8dp"
                            android:gravity="center_vertical"
                            android:text="@{showProfileViewModel.rating.rating.toString()}"
                            android:textAppearance="@style/AppTheme.Rating"
                            android:textSize="12sp" />

                    </LinearLayout>

                    <TextView
                        android:id="@+id/user_name_label"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="16dp"
                        android:text="@string/full_name_label"
                        android:textColor="@color/colorAccent" />

                    <TextView
                        android:id="@+id/user_name"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"

                        android:hint="@string/full_name"
                        android:text="@{showProfileViewModel.user.name}"
                        android:textAllCaps="false"
                        android:textColor="#000000"
                        android:textSize="20sp" />

                    <TextView
                        android:id="@+id/user_birth_date_label"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="16dp"
                        android:text="@string/birthdate_label"
                        android:textColor="@color/colorAccent" />

                    <TextView
                        android:id="@+id/user_birth_date"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="10dp"
                        android:autofillHints=""
                        android:hint="@string/birth_date"
                        android:text="@{showProfileViewModel.user.birthdate}"
                        android:textAllCaps="false"
                        android:textColor="#000000"
                        android:textSize="20sp" />

                    <TextView
                        android:id="@+id/user_nickname_label"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="16dp"
                        android:text="@string/nickname_label"
                        android:textColor="@color/colorAccent" />

                    <TextView
                        android:id="@+id/user_nickname"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:hint="@string/nickname"
                        android:text="@{showProfileViewModel.user.nickname}"
                        android:textAllCaps="false"
                        android:textColor="#000000"
                        android:textSize="20sp" />

                    <TextView
                        android:id="@+id/user_email_label"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="16dp"
                        android:text="@string/email_label"
                        android:textColor="@color/colorAccent" />

                    <TextView
                        android:id="@+id/user_email"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:hint="@string/email_address"
                        android:inputType="textEmailAddress"
                        android:text="@{showProfileViewModel.user.email}"
                        android:textAllCaps="false"
                        android:textColor="#000000"
                        android:textSize="20sp"
                        tools:ignore="TextViewEdits" />

                    <TextView
                        android:id="@+id/user_phonenum_label"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="16dp"
                        android:text="@string/phonenum_label"
                        android:textColor="@color/colorAccent" />

                    <TextView
                        android:id="@+id/user_phonenum"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:hint="@string/phonenum"
                        android:inputType="textEmailAddress"
                        android:text="@{showProfileViewModel.user.phonenumber}"
                        android:textAllCaps="false"
                        android:textColor="#000000"
                        android:textSize="20sp"
                        tools:ignore="TextViewEdits" />

                    <TextView
                        android:id="@+id/user_location_label"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="16dp"
                        android:text="@string/itemLocation"
                        android:textColor="@color/colorAccent" />

                    <TextView
                        android:id="@+id/user_location"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:hint="@string/itemLocation"
                        android:text="@{showProfileViewModel.user.location}"
                        android:textAllCaps="false"
                        android:textColor="#000000"
                        android:textSize="20sp" />

                    <RelativeLayout
                        android:id="@+id/map_layout"
                        android:layout_width="match_parent"
                        android:layout_height="300dp"
                        android:layout_marginStart="2dp"
                        android:layout_marginTop="16dp"
                        android:layout_marginBottom="12dp">

                        <fragment
                            android:id="@+id/mainMap"
                            android:name="com.google.android.gms.maps.SupportMapFragment"
                            android:layout_width="match_parent"
                            android:layout_height="match_parent" />

                        <!-- this transparent image is needed to scroll the image vertically -->
                        <ImageView
                            android:id="@+id/transparent_image"
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            android:src="@color/transparent" />

                    </RelativeLayout>

                </LinearLayout>

                <!--<ScrollView
                    android:id="@+id/main_scrollview"
                    android:layout_width="match_parent"
                    android:layout_height="0dp"
                    android:layout_weight="2"
                    android:padding="16dp">


                </ScrollView>-->

            </LinearLayout>

        </ScrollView>

    </androidx.swiperefreshlayout.widget.SwipeRefreshLayout>




</layout>
